<?php
// $Id$

/**
 * Theme links for hook link display. Honours global lister settings:
 * - collate - present as single add to list link
 *
 * If user can only add to one list type then display link to add to list type. There is an
 * optional step where the user can choose which list to add to or to add to a new list. If
 * omitted and user can have more than one list then add to new list if none exists or most recent
 * if user already has one or more lists.
 */
function theme_lister_links($list_handlers, $entity, $options) {
  global $user;
  $links = array();
  $global_settings = lister_get_global_configuration();

  $single_list_type = count($list_handlers) == 1;

  // if option to collate links and more than 1 list type then will need list selection page
  $params = $entity->to_params();
  $params['destination'] = lister_get_destination();

  if (!$single_list_type && $options['collate_links']) {
    // more than one list type - present single link so list selection page
    $links[] = array('href' => 'lister/wf/select/all', 'title' => t('Add to list'), 'query' => $params);
  }
  else {
    foreach ($list_handlers as $id => $list_handler) {
      // $list_handler = lister_get_handler($id);
      if ($list_handler->get_max_per_user() == 1) {
        if ($list_handler->get_add_entry_form() <> 'required') {
          // if instant add - i.e. no confirmation form - need embedded form w/ JS to present as standard link
          module_load_include('inc', 'lister', 'lister.page');
          $links[] = array('title' => drupal_get_form('lister_list_selection_type_form', $entity, $list_handler, $user, array('inline' => TRUE)), 'html' => TRUE);
        }
        else {
          // no selection required, show form entry/confirmation page
          $links[] = array('href' => 'lister/wf/select/'.$id, 'title' => t('Add to @list_conf_title', array('@list_conf_title' => $list_handler->conf['label'])), 'query' => $params);
        }
      }
      else {
        $links[] = array('href' => 'lister/wf/select/'.$id, 'title' => t('Add to @list_conf_title', array('@list_conf_title' => $list_handler->conf['label'])), 'query' => $params);
      }
    }
  }
  return $links;
}

/**
 * Theme function for 'lister_list_selection_type_form'.
 */
function theme_lister_list_selection_type_form(&$form) {
  if ($form['#options']['inline']) {
    // apply inline theming

    // render by default as link and let JS if enabled trigger submit.
    $entity = $form['#entity'];
    $params = $entity->to_params();
    $params['destination'] = lister_get_destination();

    $list_handler = $form['#list_handler'];

    $output = '';
    foreach (element_children($form['submits']) as $submit_id) {
      $submit =& $form['submits'][$submit_id];
      $submit['#attributes']['style'] = 'display: none';
      $output .= drupal_render($submit);

      $title = !empty($submit['#direct']) ? '...' : $submit['#value'];
      $form['submits'][$submit_id.'_link'] = array(
        '#type' => 'item',
        '#value' => l($title, 'lister/wf/select/'.$list_handler->get_id(), array('query' => $params, 'attributes' => array('class' => 'lister-link-submit'))),
      );
      $output .= drupal_render($form['submits'][$submit_id.'_link']);
    }
    $output .= drupal_render($form);
    drupal_add_css(drupal_get_path('module', 'lister') .'/theme/lister.css', 'module');
    drupal_add_js(drupal_get_path('module', 'lister') . '/theme/lister.js');
    return $output;
  }
}

function theme_lister_form_ops($form) {
  return theme('links', $form['#value']);
}

/**
 * Features:
 * - Flattens the entry properties so they can be bulk edited.
 * - Adds draggable ordering.
 * - Ops for editing and deleting.
 */
function theme_lister_edit_list($form) {
  $output = '';

  $list = $form['#list'];

  $output .= drupal_render($form['title']);

  $header = array(t('Entry'));
  $rows = array();

  foreach (element_children($form['entries']) as $entry_id) {
    $entry_form =& $form['entries'][$entry_id];

    $row = array();
    $row[] = drupal_render($entry_form['entity']);

    $header_index = count($row);
    foreach (element_children($entry_form['properties']) as $element_id) {
      $property_form =& $entry_form['properties'][$element_id];
      $header[$header_index++] = $property_form['#title'];
      unset($property_form['#title']);
      unset($property_form['#description']);
      $row[] = drupal_render($property_form);
    }

    unset($entry_form['order']['#title']);
    $row[] = drupal_render($entry_form['ops']);
    $row[] = drupal_render($entry_form['order']);
    // $row[] = theme('links', $ops);

    $rows[] = array(
      'data' => $row,
      'class' => 'draggable',
    );
  }

  $header = array_merge($header, array(t('Ops'), t('Weight')));

  $output .= theme('table', $header, $rows, array('id' => 'lister-edit-list-table'));

  drupal_add_tabledrag('lister-edit-list-table', 'order', 'sibling', 'lister-entry-order');

  $output .= drupal_render($form);

  return $output;
}

/**
 * Preprocess for list display.
 */
function template_preprocess_lister_list(&$variables) {
  $variables['entries'] = $variables['list']->get_entries();
  $variables['rendered_entries'] = array();
  foreach ($variables['entries'] as $entry) {
    $variables['rendered_entries'][] = $entry->display();
  }
}

/**
 * Preprocess for list page display.
 */
function template_preprocess_lister_list_page(&$variables) {
  $list = $variables['list'];
  $variables['list_content'] = $list->display();
  $variables['list_title'] = $list->get_title();
}

/**
 * Preprocess for edit list.
 */
function template_preprocess_lister_lists(&$variables) {
  $variables['list_sets'] = array();
  foreach ($variables['list_confs'] as $id => $conf) {
    $handler = lister_get_handler($id);
    $lists = $handler->get_user_lists($variables['user']);
    $rows = array();
    foreach ($lists as $list) {
      $list_title = l($list->get_title(), $list->get_url());
      $ops = array(
        'list_edit' =>  array('title' => t('edit'), 'href' => 'lister/list/' .$id. '/' .$list->id. '/edit', 'query' => drupal_get_destination()),
        'list_delete' =>  array('title' => t('delete'), 'href' => 'lister/list/'.$id.'/'.$list->id .'/delete', 'query' => drupal_get_destination()),
      );
      $stats = $list->get_stats();

      $rows[] = array($list_title, $stats['count'], theme('links', $ops));
    }


    $header = array(t('List'), t('Size'), t('Ops'));

    $list_table = theme('table', $header, $rows);

    $variables['list_sets'][] = array('title' => !empty($conf['group_label']) ? $conf['group_label'] : $conf['label'], 'lists' => $list_table);
  }
}
