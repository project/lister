(function ($) {
  Drupal.behaviors.lister = function(context) {
    $('a.lister-link-submit:not(.lister-link-processed)').addClass('lister-link-processed').click(function() {
      $(this).parent().prev().click();
      return false;
    });
  };
})(jQuery);
