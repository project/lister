<?php
// $Id$

/**
 * @file
 * Utility functions for lister including class definitions.
 */

/*****************************************************************************
 * Configuiration Managment
 ****************************************************************************/
/**
 * Get all the configurations. Each configuration is stored as a variable for easy export via features.
 */
function lister_get_configurations() {
  global $conf;
  $configurations = array();
  $default = lister_get_default_configuration();

  foreach (array_keys($conf) as $key) {
    // Find variables that have the module prefix
    if (strpos($key, 'lister_') === 0 && $key != 'lister_settings') {
      $configuration = variable_get($key, NULL);
      if(!empty($configuration)) {
        $configurations[$configuration['id']] = array_merge($default, $configuration);
      }
    }
  }
  return $configurations;
}

/**
 * Set a single configuration.
 */
function lister_set_configuration($id, $settings = NULL) {
  if($settings) {
    variable_set('lister_'.$id, $settings);
  } else {
    variable_del('lister_'.$id);
  }
}

/**
 * Default configuration.
 *
 * @todo Guard against lister_simple_handler not being available.
 */
function lister_get_default_configuration() {
  return array(
    'id' => NULL,
    'label' => '',
    'handler_id' => 'lister_simple_handler',
    'add_list_form' => 'always',
    'add_entry_form' => 'always',
    'new_entry_position' => 'tail',
    'duplicates' => 'no',
    'max_list_size' => 20,
    'full_list_policy' => 'disable',
    'add_entry_confirmation_page' => FALSE
  );
}

/**
 * Get a single configuration identified by it's id.
 */
function lister_get_configuration($id = NULL) {
  $configurations = lister_get_configurations();

  if (empty($id)) {
    return lister_get_default_configuration();
  }
  elseif(!empty($configurations[$id])) {
    return $configurations[$id];
  }
  else {
    return FALSE;
  }
}

/**
 * Get the global lister settings.
 */
function lister_get_global_configuration() {
  return variable_get('lister_settings', array('collate_links' => FALSE, 'lots_of_lists' => 10));
}

/*****************************************************************************
 * Pluggable list handler management.
 ****************************************************************************/
/**
 * Get pluggable list handlers using CTools plugins.
 */
function lister_get_list_handler_plugins() {
  ctools_include('plugins');
  return ctools_get_plugins('lister', 'list_handlers');
}

/**
 * Get single pluggable list handler using CTools plugins.
 */
function lister_get_list_handler_plugin($id) {
  ctools_include('plugins');
  return ctools_get_plugins('lister', 'list_handlers', $id);
}

/**
 * Gets a configured list handler instance given the id.
 */
function lister_get_handler($list_conf_id) {
  $conf = lister_get_configuration($list_conf_id);
  $plugin = lister_get_list_handler_plugin($conf['handler_id']);

  return new $plugin['handler']($conf);
}

/**
 * Gets a configured list handler instance given the configuration array.
 */
function lister_get_handler_for_conf($conf) {
  $plugin = lister_get_list_handler_plugin($conf['handler_id']);
  return new $plugin['handler']($conf);
}

/**
 * Return the set of list handler instances that are configured to support the given entity.
 */
function _lister_get_handlers_for_entity($type, $object) {
  $configurations = lister_get_configurations();
  $handlers = array();
  foreach ($configurations as $id => $list_conf) {
    $handler = lister_get_handler($id);
    if ($entity_handler = $handler->get_entity_handler($type, $object)) {
      $handlers[$id] = $handler;
    }
  }
  return $handlers;
}

/*****************************************************************************
 * Classes.
 ****************************************************************************/
/**
 * The base list class. Put default business logic in here. All persistence
 * and handler-specific business logic is handled by a list handler.
 */
class lister_list {
  var $handler;
  var $id;
  var $properties;
  var $entries;

  var $update_required = FALSE;

  function __construct($handler, $id, $properties) {
    $this->handler = $handler;
    $this->id = $id;
    $this->properties = $properties;
  }

  /**
   * Add a list entry to the list. This function tests the various conditions
   * to determine whether and how the entry should be added. The entry is added
   * by the list handler.
   *
   * @param $entry_properties
   *  An array with entity and user elements.
   *
   * @return
   *  A list_entry object as returned by the list handler.
   */
  function add_entry($entry_properties) {
    $entry = $this->get_entity_entry($entry_properties['entity']);

    if (!$entry || $this->handler->conf['duplicates'] == 'yes') {
      // check size limit
      $max_list_size = $this->handler->get_max_list_size();
      if (empty($max_list_size) || $max_list_size > $this->get_size()) {
        // ok to add
        $entry = $this->handler->add_list_entry($this, $entry_properties);
        $entry->status = lister_handler::ADD_ENTRY_STATUS_NEW;
        return $entry;
      }
      else {
        // apply full list policy
        if ($this->handler->conf['full_list_policy'] == 'disable') {
          // can't add - return a null entry
          $entry = $this->handler->make_list_entry($this, $entry_properties);
          $entry->status = lister_handler::ADD_ENTRY_STATUS_FULL;
          return $entry;
        } elseif ($this->handler->conf['full_list_policy'] == 'pop_off') {
          // pop off bottom
          $entries = $this->get_entries();
          $last_entry = $this->handler->conf['new_entry_position'] == 'head' ? end($entries) : reset($entries);
          $this->remove_entry($last_entry);
          $entry = $this->handler->add_list_entry($this, $entry_properties);
          $entry->status = lister_handler::ADD_ENTRY_STATUS_REPLACE;
          return $entry;
        }
      }
    } else {
      // already in list - strategy?
      if ($this->handler->conf['duplicates'] <> 'move_to_new') {
        // move matching existing entry to top of list
        $this->move_to_top($entry);
        $entry->status = lister_handler::ADD_ENTRY_STATUS_TOTOP;
      } else {
        // duplicates prohibited - do nothing
      }
      return $entry;
    }
  }

  /**
   * Update an entry. Delegates to the list handler.
   */
  function update_entry($entry) {
    $this->handler->update_list_entry($this, $entry);
  }

  /**
   * Remove an entry. Delegates to the list handler.
   */
  function remove_entry($entry) {
    $this->handler->remove_list_entry($this, $entry);
  }

  /**
   * Get the list title. Delegates to the list handler.
   */
  function get_title() {
    return $this->handler->get_list_title($this);
  }

  /**
   * Set the list title. Delegates to the list handler.
   */
  function set_title($title) {
    // Delegate to the list handler.
    $this->handler->set_list_title($this, $title);
  }

  /**
   * Get the number of entries in the list.
   */
  function get_size() {
    $stats = $this->get_stats();
    return $stats['count'];
  }

  /**
   * Get any available stats about the list. Delegates to the list handler.
   */
  function get_stats() {
    return $this->handler->get_list_stats($this);
  }

  /**
   * Get entries. If entries have not yet been loaded in this list instance
   * or a refresh is required the entries are got from the handler.
   *
   * @return
   *  An array of list_entry objects keyed by entry id.
   */
  function get_entries($refresh = FALSE) {
    if (!isset($this->entries) || $refresh) {
      $this->entries = $this->handler->get_list_entries($this);
    }
    return $this->entries;
  }

  /**
   * Get a single entry for a given entry id.
   */
  function get_entry($id) {
    $this->get_entries();
    if (array_key_exists($id, $this->entries)) {
      return $this->entries[$id];
    }
  }

  /**
   * Get the first entry for a given entity.
   */
  function get_entity_entry($entity) {
    $this->get_entries();
    foreach ($this->entries as $id => $entry) {
      if ($entry->entity->equals($entity)) {
        return $entry;
      }
    }
  }

  /**
   * Get the url of the list view page. Delegates to the list handler.
   *
   * @todo hmmmm.
   */
  function get_url() {
    return $this->handler->get_url($this);
  }

  /**
   * Get a rendered list. Delegates to the list handler.
   */
  function display() {
    return $this->handler->display($this);
  }

  /**
   * Save the list. Delegates to the list handler.
   *
   * @return The list object after saving.
   *
   * @todo Clarify when this should be called.
   */
  function save() {
    return $this->handler->save_list($this);
  }

  /**
   * Move an entry to the top of the list. Delegates to the list handler.
   *
   * @return The list entry object after it has been moved to the top of the list.
   */
  function move_to_top($entry) {
    return $this->handler->move_to_top($this, $entry);
  }

  /**
   * Get the list id.
   */
  function get_id() {
    return $this->id;
  }

  /**
   * Set list details. Delegates to the list handler.
   */
  function set_details($details) {
    return $this->handler->set_list_details($this, $details);
  }
}

/*****************************************************************************
 * Entity related functions and classes.
 *
 * The entity class is a wrapper around any Drupal (or custom) entity such as
 * nodes, users, and comments.
 ****************************************************************************/

/**
 * Wrap an object of given type in an lister_entity object.
 */
function lister_get_entity($type, $object) {
  if ($handler = lister_get_entity_handler($type)) {
    return $handler->construct_entity_obj($object);
  }
}

/**
 * Get pluggable entity handlers using CTools plugins.
 */
function lister_get_entity_handlers() {
  ctools_include('plugins');
  return ctools_get_plugins('lister', 'entity_handlers');
}

/**
 * Get a handler object for the given entity type.
 */
function lister_get_entity_handler($type) {
  static $handlers_by_type;

  if (!$handlers_by_type) {
    $handlers_by_type = array();
    $handlers = lister_get_entity_handlers();
    foreach ($handlers as $handler) {
      $handlers_by_type[$handler['type']] = $handler;
    }
  }

  if (isset($handlers_by_type[$type])) {
    return new $handlers_by_type[$type]['handler']();
  }
}

/**
 * The lister entity wrapper class. All functionality is delegated to
 * entity handlers.
 */
class lister_entity {
  var $id;
  var $handler;

  /**
   * Constructor.
   *
   * @param $id
   *  The id of the original entity.
   *
   * @param $handler
   *  An instance of an entity handler that can handle the original entity.
   */
  function __construct($id, $handler) {
    $this->id = $id;
    $this->handler = $handler;
  }
  function get_title() {
    return $this->handler->get_title($this->id);
  }
  function display($context = 'edit') {
    return $this->handler->display($this->id, $context);
  }
  function to_params() {
    return $this->handler->to_params($this->id);
  }
  function get_id() {
    return $this->id;
  }
  function get_type() {
    return $this->handler->get_type($this->id);
  }
  function get_object() {
    return $this->handler->get_object($this->id);
  }
  function get_url() {
    return $this->handler->get_url($this->id);
  }
  function equals($entity) {
    return $this->id == $entity->id;
  }
}

/**
 * The lister_entity_handler class. Most methods should be overridden by
 * sub classes.
 *
 * @abstract
 */
class lister_entity_handler {
  var $settings;

  function __construct($settings = array()) {
    $this->settings = $settings;
  }

  function get_title($id) {}
  function display($id) {}

  /**
   * Factory method to create a lister_entity instance. Subclasses could create
   * instances of specialised subclasses of lister_entity if required.
   */
  function construct_entity($id) {
    return new lister_entity($id, $this);
  }

  /**
   * Factory method to create a lister_entity instance from an object.
   *
   * @abstract
   */
  function construct_entity_obj($object) {}

  /**
   * Get parameters suitable for including in a url.
   *
   * @return
   *  An array of 'et' => type and 'eid' => entity id.
   *
   * @abstract
   */
  function to_params($id) {}

  function get_type($id) {}
  function get_object($id) {}
  function get_url($id) {}

  /**
   * Get a form for any configuration required by this entity handler.
   */
  function get_settings_form($form_state) {
    return array();
  }

  /**
   * Process the entity handler configuration for and return the processed values.
   *
   * @todo - this is not used - it should be.
   */
  function submit_settings_form($form, &$form_state) {
    return array();
  }

  /**
   * Tests whether the entity handler is configured to handle the given entity.
   *
   * @abstract
   */
  function is_entity_supported($type, $object) {}
}

/**
 * Validation routine to strip zero (unselected) values from a checkboxes selection.
 */
function _lister_checkboxes_validate($element, &$form_state) {
  form_set_value($element, $element['#value'], $form_state);
}

/**
 * Base class for list entries.
 */
class lister_list_entry {
  var $list;
  var $entity;
  var $id;
  var $properties;
  var $order;
  var $status;

  var $update_required = FALSE;

  function __construct($list, $id, $entity, $order = 0, $properties = array()) {
    $this->list = $list;
    $this->id = $id;
    $this->entity = $entity;
    $this->order = $order;
    $this->properties = $properties;
  }

  /**
   * Default to entity title.
   */
  function get_title() {
    return $this->entity->get_title();
  }

  /**
   * Default to entity display.
   */
  function display() {
    return $this->entity->display();
  }

  function get_id() {
    return $this->id;
  }

  function get_form() {
    $form = $this->list->handler->get_entry_form($list, $this);
    return $form;
  }

  function submit_form($form, $form_values) {
    return $form_values;
  }

  function set_order($order) {
    if ($this->order <> $order) {
      $this->order = $order;
      $this->update_required = TRUE;
    }
  }

  function set_details($details) {
    if ($this->properties <> $details) {
      $this->properties = $details;
      $this->update_required = TRUE;
    }
  }
}

/**
 * List handler abstract class.
 *
 * @todo Document this!
 *
 * @abstract
 */
class lister_handler {
  const ADD_ENTRY_STATUS_NEW = 1;
  const ADD_ENTRY_STATUS_FULL = 2;
  const ADD_ENTRY_STATUS_TOTOP = 3;
  const ADD_ENTRY_STATUS_REPLACE = 4;

  var $conf;
  function __construct($conf) {
    $this->conf = $conf;
  }

  function create_list($properties) {}

  function save_list() {}

  function delete_list($list) {}

  function get_id() {
    return $this->conf['id'];
  }

  function get_max_per_user() {
    return $this->conf['max_per_user'];
  }

  function get_max_list_size() {
    return $this->conf['max_list_size'];
  }

  function get_add_entry_form() {
    return $this->conf['add_entry_form'];
  }

  function get_add_list_form() {
    return $this->conf['add_list_form'];
  }

  function get_lists_for_entity($entity) {}

  /**
   * remove a list entry from the list
   * @abstract
   */
  function remove_list_entry($list, $list_entry) {}

  /**
   * add a list entry to the list
   * @abstract
   */
  function add_list_entry($list = NULL, $list_entry) {}

  function make_list_entry($list, $entry_properties) {
    return new lister_list_entry($list, NULL, $entry_properties['entity'], 0, $entry_properties['properties']);
  }

  /**
   * Add entry to a new list.
   */
  function add_entry_to_new_list($list_properties, $entry_properties) {
    // set defaults
    if (!isset($list_properties['title'])) {
      $list_properties['title'] = $this->generate_list_title();
    }
    $list = $this->create_list($list_properties);

    return $list->add_entry($entry_properties);
  }

  /**
   * add a list entry to the default list or create one if allowed.
   */
  function add_entry_to_default_list($list_entry) {
    $user = $list_entry['user'];
    $list = $this->get_default_list($user, TRUE);

    return $list->add_entry($list_entry);
  }

  /**
   * Get the default user list - default to the first one returned.
   */
  function get_default_list($user, $create = FALSE) {
    $lists = $this->get_user_lists($user, 1);
    if (!empty($lists)) {
      return reset($lists);
    }
    elseif ($create) {
      $list_properties = array('title' => $this->generate_list_title(), 'user' => $user);
      return $this->create_list($list_properties);
    }
  }

  /**
   * Update a list entry. Sets a flag that the entry has been updated. Does
   * not determine whether the entry has changed but subclasses may choose to
   * do so depeding on entry properties or entry order.
   */
  function update_list_entry($list, $list_entry) {
    $entries =& $list->get_entries();
    $entries[$list_entry->id] = $list_entry;
    $entries[$list_entry->id]->update_required = TRUE;
  }

  function remove_entry($list, $list_entry) {}

  /**
   * Return a form for inputting entry details.
   */
  function get_entry_form($list, $entry = NULL) {}

  /**
   * Return form for inputting list details.
   */
  function get_list_form($list = NULL) {
    $form = array();
    $form['title'] = array(
      '#title' => t('Title'),
      '#type' => 'textfield',
    );

    if ($list) {
      $form['title']['#default_value'] = $list->get_title();
    }

    return $form;
  }

  function submit_list_form($list, $values) {
    return $values;
  }

  function get_list_entries($list) {}

  function move_to_top($list, $entry) {
    $entries = $list->get_entries();
    unset($entries[$entry->id]);
    array_unshift($entries, $entry);
    $order = 1;
    foreach ($entries as $entry) {
      if ($entry->order <> $order) {
        $entry->order = $order;
        $entry->update_required = TRUE;
      }
      $order += 1;
    }
    $list->save();
  }

  /**
   * Generate a list title.
   */
  function generate_list_title() {
    return t('New @list_type_label', array('@list_type_label' => $this->conf['label']));
  }

  function get_list_title($list) {
    return $list->properties['title'];
  }

  function set_list_title($list, $title) {
    $list->properties['title'] = $title;
  }


  function display($list) {
    return theme('lister_list', $list);
  }

  /**
   * Default to any pluggable entity handler. Sub classes can either reduce the set
   * or return their own specialised handlers.
   */
  function get_supported_entity_handlers() {
    return lister_get_entity_handlers();
  }

  /**
   * Get a configured instance of an entity handler. Gives the list handler a chance
   * to configure the handler.
   */
  function get_supported_entity_handler($id) {
    $entity_handler_defs = $this->get_supported_entity_handlers();
    if (!empty($entity_handler_defs[$id])) {
      $entity_handler_settings = $this->conf['entity_support'][$id]['settings'];
      return new $entity_handler_defs[$id]['handler']($entity_handler_settings);
    }
  }

  /**
   * Get an entity handler configured to support the given entity.
   */
  function get_entity_handler($type, $object = NULL) {
    $entity_handler_defs = $this->get_supported_entity_handlers();
    if (isset($this->conf['entity_support']) && !empty($this->conf['entity_support'])) {
      foreach ($this->conf['entity_support'] as $id => $entity_handler_conf) {
        // test to see if enabled for this configuration and of the right type
        if (!empty($entity_handler_conf['enabled']) && $entity_handler_defs[$id]['type'] == $type) {
          // create a configured instance of the entity handler
          $entity_handler = $this->get_supported_entity_handler($id);
          if ($object) {
            // return handler if object is supported
            if ($entity_handler->is_entity_supported($type, $object)) {
              return $entity_handler;
            }
          }
          else {
            return $entity_handler;
          }
        }
      }
    }
  }

  function get_url($list) {
    return 'lister/list/'.$this->get_id().'/'.$list->get_id();
  }

  function set_list_details($list, $details) {
    if ($list->properties <> $details) {
      $list->properties = $details;
      $list->update_required = TRUE;
    }
  }

  function get_settings_form($settings) {}

  function get_settings_override_form(&$form, $settings) {}
}
