<?php
// $Id$
include_once dirname(__FILE__) .'/lister.inc';

/**
 * @file
 */

/**
 * Menu handler for add entry workflow. The 'select' step encapsulates the
 * multi-step add entry form. The 'done' step returns an action completion landing
 * page. Entity information is passed via url parameters.
 */
function lister_workflow($step, $list_conf_id) {
  global $user;

  if ($step == 'select') {
    // get entity and relevant list confs for initial form
    if($entity = lister_get_entity_from_url()) {
      // select list - use form as allows for most flexible selection methods - e.g. autocomplete, checkbox etc
      $list_handlers = lister_get_handlers_for_entity($entity->get_type(), $entity->get_object());
    }

    if ($list_conf_id <> 'all' && isset($list_handlers[$list_conf_id])) {
      // single specified type form
      $list_handler = $list_handlers[$list_conf_id];
      $options = array('list_selection' => TRUE, 'subform' => FALSE, 'inline' => FALSE);
      return drupal_get_form('lister_list_selection_type_form', $entity, $list_handler, $user, $options);
    }
    else {
      // all types form
      $options = array('list_selection' => TRUE, 'max_selections' => 7);
      return drupal_get_form('lister_list_selection_form', $entity, $list_handlers, $user, $options);
    }
  }
  elseif ($step == 'done') {
    // Entry add actions have been completed - get action statuses from url parameters.
    // Using params rather than drupal_set_message so that messages are consistent between
    // refreshes and back steps.
    $entity = lister_get_entity_from_url();
    $list_conf_id = $list_conf_id <> 'all' ? $list_conf_id : $_GET['lt'];
    $list_handler = lister_get_handler($list_conf_id);
    $actions = array();

    $mcs = is_array($_GET['mcs']) ? $_GET['mcs'] : array($_GET['mcs']);
    foreach ($mcs as $message_code) {
      $actions[] = lister_add_entry_confirmation_message_decode($list_handler, $message_code);
    }
    return lister_list_wf_complete_page($list_conf_id, $entity, $actions);
  }
}

/**
 * Optional landing page after list entries have been added.
 *
 * @param $actions
 *  An array of action status arrays.
 */
function lister_list_wf_complete_page($list_conf_id, $entity = NULL, $actions) {
  // Output action messages.
  foreach ($actions as $action) {
    $message = lister_add_entry_confirmation_message($action['list'], $entity, $action['status'], isset($action['entry']) ? $action['entry'] : NULL);
    drupal_set_message($message);
  }

  // Display a back link - useful if got here after a multistep form.
  if ($destination = $_REQUEST['destination']) {
    // Do not redirect to an absolute URL originating from user input.
    $colonpos = strpos($destination, ':');
    $absolute = ($colonpos !== FALSE && !preg_match('![/?#]!', substr($destination, 0, $colonpos)));
    if (!$absolute) {
      extract(parse_url(urldecode($destination)));
    }
    $output = l(t('Back'), $path, array('query' => $query, 'fragment' => $fragment, 'absolute' => TRUE));
  }

  return $output;
}

/**
 * Defines the main add entry form which includes steps for selecting the list to add to,
 * entering new list details, entering entry details, and completing the action.
 * Different list configurations are included as subforms.
 *
 * @param $entity
 *  An lister_entity object.
 * @param $list_confs
 *  An array of list type configurations to include.
 * @param $user
 *  The user for whom to add the entry.
 * @param $options
 *  An array of options including:
 *  - 'list_selection' => Set to TRUE to present full list selection form; or
 *      FALSE to render list selection as submit buttons suitable for direct
 *      add-to-list actions as used by lister_ui.
 *  - 'max_selections' => Maximum number of lists to display (per list type).
 *  - 'theme' => Top level form theme function to use.
 */
function lister_list_selection_form($form_state, $entity = NULL, $list_handlers = array(), $user = NULL, $options = array()) {
  global $user;
  $form = array();

  // set form constants
  $form['#entity'] = $entity;
  $form['#list_handlers'] = $list_handlers;
  $form['#user'] = $user;

  if (empty($form_state['storage'])) {
    $params = $entity->to_params();
    $params['destination'] = lister_get_destination();
    $form['#action'] = url('lister/wf/select/all', array('query' => $params));
  }

  // set top level form attributes.
  $form['#attributes'] = array('class' => 'lister-selection');
  if (!empty($options)) {
    $form['#theme'] = $options['theme'];
  }

  // container element for list type subforms.
  $form['list_types'] = array(
    '#tree' => TRUE,
  );

  if ($id = $form_state['storage']['list_conf_id']) {
    // After first form step list type has been chosen.
    $list_handler = $form['#list_handlers'][$id];
    $subform_options = array('subform' => FALSE, 'inline' => FALSE);
    $form['list_types'][$id]['subform'] = lister_list_selection_type_form($form_state, $entity, $list_handler, $user, $options);
  } else {
    // First step of form include sub forms for each list type.
    foreach ($form['#list_handlers'] as $id => $list_handler) {
      $form['list_types'][$id] = array(
        '#tree' => TRUE,
        '#type' => 'fieldset',
        '#title' => $list_handler->conf['label'],
      );
      $subform_options = array('subform' => FALSE, 'inline' => TRUE);
      $form['list_types'][$id]['subform'] = lister_list_selection_type_form($form_state, $entity, $list_handler, $user, $options);
    }
  }

  return $form;
}

/**
 * Get the add entry form for a single list type.
 *
 * This is a multi-step form. The steps are determined by list type configuration settings and user selections.
 * Steps can be combined into fewer steps.
 *
 * @param $options
 *  An array of options including:
 *  - 'list_selection' => Set to TRUE to present full list selection form; or
 *      FALSE to render list selection as submit buttons suitable for direct
 *      add-to-list actions as used by lister_ui.
 *  - 'max_selections' => Maximum number of lists to display (per list type).
 *  - 'inline' => TRUE if the form is to be displayed inline e.g. in list of links.
 *  - 'subform' => TRUE if this form is included in another form.
 */
function lister_list_selection_type_form(&$form_state, $entity, $list_handler, $user, $options = array(), $steps = array('list_selection')) {
  // set option defaults
  $options = array_merge(array('inline' => FALSE, 'subform' => FALSE), $options);

  $form = array();

  // set form constants.
  $form['#entity'] = $entity;
  $form['#list_handler'] = $list_handler;
  $form['#user'] = $user;
  $form['#options'] = $options;

  // Set top form level attributes.
  $form['#attributes'] = array('class' => 'lister-selection');
  // if (!empty($options['theme'])) {
  //   $form['#theme'] = $options['theme'];
  // }

  if (empty($form_state['storage'])) {
    // set form action url on the first step.
    $params = $entity->to_params();
    $params['destination'] = lister_get_destination();
    $form['#action'] = url('lister/wf/select/'.$list_handler->get_id(), array('query' => $params));
  }

  // get the complete set of form steps for the current form_state.
  $form['#all_steps'] = _lister_get_all_form_steps($form_state, $list_handler);

  // next steps determined in previous step form submission or from function argument.
  $form['#steps'] = !empty($form_state['storage']['next_steps']) ? $form_state['storage']['next_steps'] : $steps;

  // get the steps that will remain after this form has been submitted.
  $form['#next_step_steps'] = _lister_get_next_step_steps($form['#all_steps'], $form['#steps']);

  // render steps
  foreach ($form['#steps'] as $step) {
    if ($step == 'list_selection') {
      if ($list_handler->get_max_per_user() == 1) {
        // special case - no selection necessary and no explicit "add to new" required
        $form['submits']['add_default'] = array(
          '#type' => 'submit',
          '#value' => t('Add to @list_conf_title', array('@list_conf_title' => $list_handler->conf['label'])),
        );

        // add optional detail link if entry details is an optional up-coming step
        if (isset($form['#all_steps']['entry_details']) && !$form['#all_steps']['entry_details']['required'] && !in_array('entry_details', $form['#steps'])) {
          // allow user to add entry details - optional so provide link in addition to main button action
          $form['submits']['add_default_link_detail'] = array(
            '#type' => 'submit',
            '#value' => t('Add to @list_conf_title...', array('@list_conf_title' => $list_handler->conf['label'])),
            '#direct' => 'add_default',
          );
          $form['submits']['add_default']['#next_steps'] = array('complete');
        }
      }
      else {
        // existing list selection
        $max_selections = !empty($options['max_selections']) ? $options['max_selections'] : 0;
        $lists = $list_handler->get_user_lists($user, $max_selections);
        $list_count = $list_handler->get_user_list_count($user);
        if (!empty($options['list_selection'])) {
          // Present list multiple selection - checkboxes
          $list_options = array();
          foreach ($lists as $list) {
            $list_options[$list->id] = $list->get_title();
          }

          $form['list_selection'] = array(
            '#type' => 'checkboxes',
            '#title' => t('Select @list_conf_title', array('@list_conf_title' => !empty($list_handler->conf['group_label']) ? $list_handler->conf['group_label'] : $list_handler->conf['label'])),
            '#options' => $list_options,
            '#element_validate' => array('_lister_list_selection_checkboxes_validate'),
          );

          $form['submits']['add_selection'] = array(
            '#type' => 'submit',
            '#value' => t('Add to selected @list_conf_title', array('@list_conf_title' => $list_handler->conf['label'])),
          );
        } else {
          // Present add to list buttons
          foreach ($lists as $list) {
            $form['submits']['add'.$list->get_id()] = array(
              '#type' => 'submit',
              '#value' => t('Add to @list_title', array('@list_title' => $list->get_title())),
              '#list_id' => $list->get_id(),
            );
            // add optional detail link if entry details is an optional up-coming step
            if (isset($form['#all_steps']['entry_details']) && !$form['#all_steps']['entry_details']['required'] && !in_array('entry_details', $form['#steps'])) {
              // allow user to add entry details - optional so provide link in addition to main button action
              $form['submits']['add'.$list->get_id().'_link_detail'] = array(
                '#type' => 'submit',
                '#value' => t('Add to @list_title...', array('@list_title' => $list->get_title())),
                '#list_id' => $list->get_id(),
              );
              $form['submits']['add'.$list->get_id()]['#next_steps'] = array('complete');
            }
          }
        }

        // Add link to full selection page.
        if ($list_count > count($lists)) {
          $params = $entity->to_params();
          $params['destination'] = lister_get_destination();

          $form['more_lists_link'] = array(
            '#type' => 'item',
            '#value' => l(t('More @list_conf_title lists...', array('@list_conf_title' => $list_handler->conf['label'])), 'lister/wf/select/'.$list_handler->get_id(), array('query' => $params, 'attributes' => array('class' => 'lister-link-submit'))),
          );
        }

        // Add to new list button.
        $form['submits']['add_new'] = array(
          '#type' => 'submit',
          '#value' => t('Add to new @list_conf_title', array('@list_conf_title' => $list_handler->conf['label'])),
        );
      }
    }
    elseif ($step == 'entry_details') {
      // add the entry details form.
      $form['properties'] = $list_handler->get_entry_form();

      if (empty($form['submits'])) {
        // add the default submit button
        $form['submits']['submit'] = array(
          '#type' => 'submit',
          '#value' => t('Add'),
        );
      }
    }
    elseif ($step == 'list_details') {
      // add the list details form
      $form['list_properties'] = array(
        '#tree' => TRUE,
      );

      $form['list_properties'] = array_merge($form['list_properties'], $list_handler->get_list_form());
    }
  }

  // Add a form submission button if none already provided.
  if (empty($form['submits'])) {
    $form['submits']['submit'] = array(
      '#type' => 'submit',
      '#value' => !empty($form['#next_step_steps']) && $form['#next_step_steps'][0] <> 'complete' ? t('Next') : t('Submit'),
    );
  }

  // Set a form context aware title for use by themes etc.
  if (!empty($form_state['storage']['form_title'])) {
    $form['#form_title'] = $form_state['storage']['form_title'];
  }

  $form['submits']['#weight'] = 5;

  return $form;
}

/**
 * Validation routine to strip zero (unselected) values from a checkboxes selection.
 */
function _lister_list_selection_checkboxes_validate($element, &$form_state) {
  form_set_value($element, $element['#value'], $form_state);
}

/**
 * Form handler for combined list types form.
 */
function lister_list_selection_form_submit($form, &$form_state) {
  return lister_list_selection_type_form_submit($form, $form_state);
}

/**
 * Handle add selected lists submissions.
 */
function lister_list_selection_type_form_submit($form, &$form_state) {
  if (!empty($form_state['clicked_button'])) {
    $entity = $form['#entity'];

    if (isset($form['#list_handler'])) {
      $list_handler = $form['#list_handler'];
    }
    else {
      $list_handler = lister_get_handler($form_state['clicked_button']['#parents'][1]);
    }

    $button_values = _lister_get_form_values($form, $form_state, $form_state['clicked_button']);
    $form = $button_values['form'];

    $form_state['storage']['next_steps'] = !empty($form_state['clicked_button']['#next_steps']) ? $form_state['clicked_button']['#next_steps'] : array();
    $form_state['storage']['list_conf_id'] = $list_handler->get_id();

    // form storage for each step
    foreach ($form['#steps'] as $step) {
      if ($step == 'list_selection') {
        if ($button_values['action'] == 'add_new') {
          $form_state['storage']['list_selection'] = 'new';
          if ($list_handler->get_add_list_form() <> 'never' && !in_array('list_details', $form['#steps'])) {
            // entry details required and not already supplied
            $form_state['storage']['next_steps'][] = 'list_details';
            $form_state['storage']['form_title'] = t('Add to new list');
          }
        }
        elseif ($button_values['action'] == 'add_default' || $button_values['action'] == 'add_default_link_detail') {
          $form_state['storage']['list_selection'] = 'default';
          if ($list_handler->get_add_entry_form() <> 'never' && !in_array('entry_details', $form['#steps'])) {
            // entry details required and not already supplied
            $form_state['storage']['next_steps'][] = 'entry_details';
            $form_state['storage']['form_title'] = t('Add to @list_type', array('@list_type' => $list_handler->conf['label']));
          }
        }
        elseif ($button_values['action'] == 'add_selection') {
          $form_state['storage']['list_selection'] = $button_values['values']['list_selection'];
          if ($list_handler->get_add_entry_form() <> 'never' && !in_array('entry_details', $form['#steps'])) {
            // entry details required and not already supplied
            $form_state['storage']['next_steps'][] = 'entry_details';
            $form_state['storage']['form_title'] = t('Add to selected lists');
          }
        }
        else {
          // add list
          $list_id = $form_state['clicked_button']['#list_id'];
          $form_state['storage']['list_selection'] = array($list_id => $list_id);
          if ($list_handler->get_add_entry_form() <> 'never' && !in_array('entry_details', $form['#steps'])) {
            // entry details required and not already supplied
            $form_state['storage']['next_steps'][] = 'entry_details';
            $list = $list_handler->get_list($list_id);
            $form_state['storage']['form_title'] = t('Add to @list', array('@list' => $list->get_title()));
          }
        }
      }
      elseif ($step == 'list_details') {
        // get list properties if provided
        $form_state['storage']['list_details'] = $list_handler->submit_list_form($form, $button_values['values']['list_properties']);
        if ($list_handler->get_add_entry_form() <> 'never' && !in_array('entry_details', $form['#steps'])) {
          // entry details required and not already supplied
          $form_state['storage']['next_steps'][] = 'entry_details';
        }
      }
      elseif ($step == 'entry_details') {
        // get entry properties if provided
        $form_state['storage']['entry_details'] = $list_handler->submit_entry_form($form, $button_values['values']);
      }
    }

    // if no futher steps - use form storage to add entries
    if (empty($form_state['storage']['next_steps']) || $form_state['storage']['next_steps'][0] == 'complete') {
      // build entry properties to add to all selected lists
      $entry_details = isset($form_state['storage']['entry_details']) ? $form_state['storage']['entry_details'] : NULL;
      $entry_properties = array('entity' => $entity, 'user' => $form['#user'], 'properties' => $entry_details);

      // get the list selection from previous step or this step if entry form was not required
      if (is_array($form_state['storage']['list_selection'])) {
        $list_selection = $form_state['storage']['list_selection'];
      }
      elseif ($form_state['storage']['list_selection'] == 'new') {
        // make new list here
        $list_properties = isset($form_state['storage']['list_details']) ? $form_state['storage']['list_details'] : NULL;
        $list_properties['user'] = $form['#user'];

        $list = $list_handler->create_list($list_properties);
        $list_selection = array($list->get_id());
      }
      elseif ($form_state['storage']['list_selection'] == 'default') {
        // make new if none yet - no list details asked for
        $list = $list_handler->get_default_list($form['#user'], TRUE);
        $list_selection = array($list->get_id());
      }

      $message_codes = array();
      $messages = array();

      // add entries to each selected list
      foreach ($list_selection as $selected_lid) {
        $list = $list_handler->get_list($selected_lid);
        $entry = $list->add_entry($entry_properties);
        $status = $entry->status;
        //
        $messages[] = lister_add_entry_confirmation_message($list, $entity, $status, $entry);
        $message_codes[] = lister_add_entry_confirmation_message_encode($list, $entry);
      }

      // either redirect to confirmation page or set a message
      if ($list_handler->conf['add_entry_confirmation_page']) {
        // construct url and redirect to landing page
        $params = array();
        $params['lt'] = $list_handler->get_id();
        $params = array_merge($params, $entity->to_params());
        $params['mcs'] = $message_codes;
        $params['destination'] = isset($form_state['storage']['destination']) ? $form_state['storage']['destination'] : $_REQUEST['destination'];
        unset($_REQUEST['destination']);
        $form_state['redirect'] = array('lister/wf/done/all', $params);
      }
      else {
        // output messages
        foreach ($messages as $message) {
          drupal_set_message($message);
        }
      }

      // clear form storage
      unset($form_state['storage']);
    }
  }
}

/**
 * Utility function to determine all steps required for the given list handler.
 * At each step, the complete set of steps may change depending on the form_state.
 *
 * @return
 *  An array of steps, each of which is an array with the following elements:
 *  - 'id' => The step id
 *  - 'label' => A label which could be used for a form progress display.
 *  - 'required' => If FALSE the step is optional.
 */
function _lister_get_all_form_steps($form_state, $list_handler) {
  // determine all steps taking into account any user selections
  $all_steps = array();

  // list selection - always required.
  $all_steps['list_selection'] = array('id' => 'list_selection', 'label' => t('Select list'), 'required' => TRUE);

  // new list details - if new list selected then depending on list type configuration a new list details step is required.
  if (isset($form_state['storage']['list_selection']) && !is_array($form_state['storage']['list_selection']) && $form_state['storage']['list_selection'] == 'new' && $list_handler->get_add_list_form() <> 'never') {
    $all_steps['list_details'] = array('id' => 'list_details', 'label' => t('Enter list details'), 'required' => $list_handler->get_add_list_form() == 'always');
  }

  // entry details
  if ($list_handler->get_add_entry_form() <> 'never') {
    $all_steps['entry_details'] = array('id' => 'entry_details', 'label' => t('Enter list entry details'), 'required' => $list_handler->get_add_entry_form() == 'always');
  }

  // completion step
  $all_steps['complete'] = array('id' => 'complete', 'label' => t('Finish'), 'required' => TRUE);

  return $all_steps;
}

/**
 * Utility function to get the set of remaining steps.
 *
 * @return
 *  An array of step ids.
 */
function _lister_get_next_step_steps($all_steps, $current_steps) {
  $next_step_steps = array_keys($all_steps);
  $last_current_step = end($current_steps);
  foreach ($all_steps as $key => $next_step) {
    array_shift($next_step_steps);
    if ($key == $last_current_step) {
      break;
    }
  }
  return $next_step_steps;
}

/**
 * Handles getting the relevant form values for a clicked button for
 * both nested and unested forms.
 */
function _lister_get_form_values($form, $form_state, $clicked_button) {
  $parents = $clicked_button['#parents'];
  $action = array_pop($parents);
  array_pop($parents);
  $values = $form_state['values'];
  foreach ($parents as $parent) {
    $values = $values[$parent];
    $form = $form[$parent];
  }
  return array('action' => $action, 'values' => $values, 'form' => $form);
}

/**
 * Encode the result of an "add entry" action for inclusion in a url.
 */
function lister_add_entry_confirmation_message_encode($list, $entry) {
  $code = $list->get_id().':'.$entry->status;
  if (isset($entry->leid)) {
    $code .= ':'.$entry->leid;
  }
  return urlencode($code);
}

/**
 * Decode the result of an "add entry" action.
 */
function lister_add_entry_confirmation_message_decode($list_handler, $code) {
  $parts = explode(':',urldecode($code));
  $list = $list_handler->get_list($parts[0]);
  $message_params = array('list' => $list, 'status' => $parts[1]);
  if (!empty($parts[2])) {
    $message_params['entry'] = $list->get_entry($parts[2]);
  }
  return $message_params;
}

/**
 * Construct the add entry confirmation message.
 */
function lister_add_entry_confirmation_message($list, $entity, $status, $entry = NULL) {
  if ($status == lister_handler::ADD_ENTRY_STATUS_FULL) {
    $message = t('<a href="@list-page">@list-title</a> is full.', array(
      '@list-page' => url($list->get_url()),
      '@list-title' => $list->get_title(),
    ));
  } elseif ($status == lister_handler::ADD_ENTRY_STATUS_NEW || $status == lister_handler::ADD_ENTRY_STATUS_REPLACE) {
    $message = t('You added <a href="@entity-page">@entity-title</a> to <a href="@list-page">@list-title</a>.', array(
      '@entity-page' => url($entity->get_url()),
      '@entity-title' => $entity->get_title(),
      '@list-page' => url($list->get_url()),
      '@list-title' => $list->get_title(),
    ));
  } elseif ($status == lister_handler::ADD_ENTRY_STATUS_TOTOP) {
    $message = t('<a href="@entity-page">@entity-title</a> was already in <a href="@list-page">@list-title</a>. It has been moved to the top.', array(
      '@entity-page' => url($entity->get_url()),
      '@entity-title' => $entity->get_title(),
      '@list-page' => url($list->get_url()),
      '@list-title' => $list->get_title(),
    ));
  }

  return $message;
}

/**
 * List user lists.
 */
function lister_my_lists_page($uid) {
  $lists_by_type = array();
  $user = user_load($uid);

  $confs = lister_get_configurations();
  foreach ($confs as $id => $conf) {
    $confs[$id] = lister_get_configuration($id);
  }

  return theme('lister_lists', $confs, $user);
}

/**
 * Handler for entity operations.
 */
function lister_entity_op($op, $list) {
  global $user;

  if ($op == 'remove') {
    $entity = lister_get_entity_from_url();
    $list->remove_entry($entry_properties);
    drupal_set_message('Removed from list.');
  }
  elseif ($op == 'edit') {
    return $list->display();
  }
  elseif ($op == 'view') {
    return $list->display();
  }

  drupal_goto();
}

/**
 * Handler for list entry operations.
 */
function lister_entry_op($op, $list, $entry_id) {
  if ($op == 'edit') {
    $entry = $list->get_entry($entry_id);
    $entry_form = drupal_get_form('lister_edit_entry_form', $list, $entry);
    return $entry_form;
  }
  elseif ($op == 'remove') {
    $entry = $list->get_entry($entry_id);
    return drupal_get_form('lister_remove_entry_form', $list, $entry);
  }
}

/**
 * Edit list entry form.
 */
function lister_edit_entry_form($form_state, $list, $entry) {
  $form = array();
  $form['properties'] = $entry->get_form();
  $form['#list'] = $list;
  $form['#entry'] = $entry;
  $form['save'] = array(
    '#type' => 'submit',
    '#value' => t('Save entry'),
  );
  return $form;
}

/**
 * Submit handler for edit list entry form.
 */
function lister_edit_entry_form_submit($form, &$form_state) {
  $list = $form['#list'];
  $entry = $form['#entry'];
  $details = $entry->submit_form($form, $form_state['values']);
  $entry->set_details($details);
  $list->save();
}

/**
 * Remove list entry form.
 */
function lister_remove_entry_form($form_state, $list, $entry) {
  $form = array('#list' => $list, '#entry' => $entry);
  $question = t('Do you want to remove @entity from @list_title?', array('@entity' => $entry->get_title(), '@list_title' => $list->get_title()));
  $path = '/lister/list/edit/wishlist/'.$list->id;
  return confirm_form($form, $question, $path);
}

/**
 * Submit handler for remove list entry form.
 */
function lister_remove_entry_form_submit($form, &$form_state) {
  $list = $form['#list'];
  $list->remove_entry($form['#entry']);
  drupal_set_message('Entry removed');
}

/**
 * Menu handler for list operations - edit/delete/show.
 */
function lister_list_op($op, $list) {
  if ($op == 'edit') {
    return drupal_get_form('lister_edit_list_form', $list);
  }
  elseif ($op == 'delete') {
    return drupal_get_form('lister_delete_list_form', $list);
  }
  elseif ($op == 'show') {
    return lister_list_page($list);
  }
  elseif ($op == 'add_entry') {
    return lister_list_add_entry_page($list);
  }
}

/**
 * Display a list page.
 */
function lister_list_page($list) {
  return theme('lister_list_page', $list);
}

function lister_delete_list_form($form_state, $list) {
  $form = array('#list' => $list);
  $question = t('Do you want to delete @list_title?', array('@list_title' => $list->label));

  return confirm_form($form, $question, '');
}

function lister_delete_list_form_submit($form, &$form_state) {
  $list = $form['#list'];
  $handler = $list->handler;
  $handler->delete_list($list);
  drupal_set_message('List deleted');
}

/**
 * Prepare form for list editing. Include entry subform.
 */
function lister_edit_list_form($form_state, $list) {
  $form = array(
    '#theme' => 'lister_edit_list'
  );

  // include list edit sub form if handler provides one.
  if ($list_sub_form = $list->handler->get_list_form($list)) {
    $form['list_sub_form'] = array(
      '#tree' => TRUE,
    );

    $form['list_sub_form'] = array_merge($form['list_sub_form'], $list_sub_form);
  }

  $form['entries'] = array(
    '#tree' => TRUE,
  );

  $form['#list'] = $list;

  $entry_form = $list->handler->get_entry_form($list);
  $form['#entry_properties'] = !empty($entry_form) ? TRUE : FALSE;

  foreach ($list->get_entries() as $entry) {
    $form['entries'][$entry->id]['entity'] = array(
      '#type' => 'item',
      '#value' => $entry->entity->display('edit'),
    );
    $form['entries'][$entry->id]['order'] = array(
      '#title' => t('Order'),
      '#type' => 'textfield',
      '#size' => 3,
      '#default_value' => $entry->order,
      '#attributes' => array('class' => 'lister-entry-order'),
    );

    $ops = array(
      'entry_remove' => array('title' => t('remove'), 'href' => 'lister/entry/remove/'.$list->handler->get_id().'/'.$list->id.'/'.$entry->id, 'query' => drupal_get_destination()),
    );

    if ($form['#entry_properties']) {
      $entry_form = $entry->get_form();
      $form['entries'][$entry->id]['properties'] = array(
        '#tree' => TRUE,
      );

      $form['entries'][$entry->id]['properties'] = array_merge($form['entries'][$entry->id]['properties'], $entry_form);
      $ops['entry_edit'] = array('title' => t('edit'), 'href' => 'lister/entry/edit/'.$list->handler->get_id().'/'.$list->id.'/'.$entry->id, 'query' => drupal_get_destination(), 'attributes' => array('class' => 'lister-entry-edit'));
    }

    $form['entries'][$entry->id]['ops'] = array(
      '#type' => 'item',
      '#theme' => 'lister_form_ops',
      '#value' => $ops,
    );


  }

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save changes'),
  );

  return $form;
}

function lister_edit_list_form_submit($form, &$form_state) {
  $list = $form['#list'];

  if (!empty($form['list_sub_form'])) {
    // update list details
    $list_details = $list->handler->submit_list_form($form, $form_state['values']['list_sub_form']);
    $list->set_details($list_details);
  }

  foreach ($form_state['values']['entries'] as $entry_id => $entry_values) {
    // update list entries
    $entry = $list->get_entry($entry_id);
    $entry->set_order($entry_values['order']);

    if (!empty($form['entries'][$entry_id]['properties'])) {
      $entry_details = $entry->submit_form($form['entries'][$entry_id]['properties'], $entry_values['properties']);
      $entry->set_details($entry_details);
    }
  }

  // everything OK - save all changes
  $list->save();
}

