<?php
// $Id$

/**
 * @file
 * Admin menu callbacks and form handling for Lister configuration.
 */

include_once dirname(__FILE__) .'/lister.inc';

/**
 * Main admin page with table of configurations.
 */
function lister_admin_page() {
  $configurations = lister_get_configurations();

  foreach ($configurations as $id => &$configuration) {
    if ($handler = lister_get_list_handler_plugin($configuration['handler_id'])) {
      $configuration['handler_label'] = $handler['label'];
    }
    else {
      $configuration['handler_label'] = t('* Missing *');
    }
  }

  return theme('lister_admin_page', $configurations);
}

/**
 * Admin form for adding/editing single configuration.
 */
function lister_settings_form(&$form_state, $id = NULL) {
  $form = array();

  $settings = lister_get_configuration($id);
  $id = $form_state['storage']['id'] ? $form_state['storage']['id'] : $id;

  $form['#configuration_id'] = $id;

  if ($id == NULL) {
    $handler_plugins = lister_get_list_handler_plugins();

    if (empty($handler_plugins)) {
      drupal_set_message(t('Please enable at least one module that includes a list handler e.g. Lister Simple List.'));
    }
    else {
      // New list type - prompt
      $form['id'] = array(
        '#title' => t('Identifier'),
        '#type' => 'textfield',
        '#description' => t('Enter a unique identifier for this list configuration.'),
        '#default_value' => $id,
        '#required' => TRUE,
      );

      $list_handler_options = array();
      foreach ($handler_plugins as $plugin_id => $plugin) {
        $list_handler_options[$plugin_id] = $plugin['label'] . '<div class="description">' . $plugin['description'] . '</div>';
      }

      $form['handler_id'] = array(
        '#type' => 'radios',
        '#title' => t('List handler'),
        '#description' => t('Select list handler'),
        '#required' => TRUE,
        '#options' => $list_handler_options,
      );

      $form['buttons']['submit'] = array(
        '#type' => 'submit',
        '#value' => t('Next'),
        '#submit' => array('lister_settings_form_add_submit'),
        '#validate' => array('lister_settings_form_add_validate'),
      );
    }
  }
  else {
    $form['#handler_id'] = $form_state['storage']['handler_id'] ? $form_state['storage']['handler_id'] : $settings['handler_id'];
    $settings['handler_id'] = $form['#handler_id'];
    $form['id'] = array(
      '#title' => t('Identifier'),
      '#type' => 'textfield',
      '#description' => t('The unique identifier for this list configuration.'),
      '#default_value' => $id,
      '#value' => $id,
      '#disabled' => TRUE,
    );
    $form['label'] = array(
      '#title' => t('Label'),
      '#type' => 'textfield',
      '#description' => t('Enter a descriptive label.'),
      '#default_value' => $settings['label'],
      '#required' => TRUE,
    );

    $form['group_label'] = array(
      '#title' => t('Group label'),
      '#type' => 'textfield',
      '#description' => t('Enter a group label.'),
      '#default_value' => $settings['group_label'],
    );

    $form['max_per_user'] = array(
      '#title' => t('Per user limit'),
      '#type' => 'textfield',
      '#description' => t('Enter the maximum number of lists that a user [with add xx-list permission] may create. Leave blank for unlimited.'),
      '#default_value' => $settings['max_per_user'],
    );

    $form['ui_settings'] = array(
      '#type' => 'fieldset',
      '#title' => t('UI settings'),
      '#collapsible' => TRUE,
    );

    $form['ui_settings']['add_list_form'] = array(
      '#title' => t('Prompt for new list details'),
      '#type' => 'radios',
      '#options' => array('never' => t('Never'), 'optional' => t('Optional'), 'always' => t('Always')),
      '#description' => t("Select whether to prompt user for new list details when entity is added to new list. If 'Optional' the user may be given the option to enter details or not."),
      '#default_value' => $settings['add_list_form'],
    );

    $form['ui_settings']['add_entry_form'] = array(
      '#title' => t('Prompt for list entry details'),
      '#type' => 'radios',
      '#options' => array('never' => t('Never'), 'optional' => t('Optional'), 'always' => t('Always')),
      '#description' => t("Select whether to prompt user for list entry details when entity is added to list. If 'Optional' the user may be given the option to enter details or not."),
      '#default_value' => $settings['add_entry_form'],
    );

    $form['ui_settings']['add_entry_confirmation_page'] = array(
      '#title' => t('Redirect to confirmation page after list entry is added'),
      '#type' => 'checkbox',
      '#default_value' => $settings['add_entry_confirmation_page'],
    );

    $form['list_behaviour'] = array(
      '#type' => 'fieldset',
      '#title' => 'List behaviour settings',
      '#collapsible' => TRUE,
    );

    $form['list_behaviour']['new_entry_position'] = array(
      '#title' => t('New list entry position'),
      '#type' => 'radios',
      '#options' => array('head' => t('Head'), 'tail' => t('Tail')),
      '#description' => t("Select whether list entries are added to the head or the tail of the list."),
      '#default_value' => $settings['new_entry_position'],
    );

    $form['list_behaviour']['duplicates'] = array(
      '#title' => t('Allow duplicate entries'),
      '#type' => 'radios',
      '#options' => array('no' => t('No'), 'move_to_new' => t('No (move to new entry position)'), 'yes' => t('Yes')),
      '#description' => t("Select whether to allow duplicate list entries. If no then no action is taken. If no (move to move to new entry position) then the existing entry is moved to the new entry position."),
      '#default_value' => $settings['duplicates'],
    );

    $form['list_behaviour']['max_list_size'] = array(
      '#title' => t('Maximum list size'),
      '#type' => 'textfield',
      '#description' => t("The maximum allowed number of list entries. Leave blank or set to 0 for unlimited list size."),
      '#default_value' => $settings['max_list_size'],
    );

    $form['list_behaviour']['full_list_policy'] = array(
      '#title' => t('Full list policy'),
      '#type' => 'radios',
      '#options' => array('disable' => t('Prevent additions'), 'pop_off' => t("Remove 'last' entry to make room")),
      '#description' => t("Select what happens when a list is full. 'Pop off' will remove the head/tail entry - e.g. if the new entry is added to the head of the list then the tail entry will be removed to make room."),
      '#default_value' => $settings['full_list_policy'],
    );

    $list_handler = lister_get_handler_for_conf($settings);
    $list_handler_settings = !empty($settings['handler_settings']) ? $settings['handler_settings'] : array();

    if ($handler_settings_sub_form = $list_handler->get_settings_form($list_handler_settings)) {
      $form['handler_settings'] = array(
        '#title' => t('List handler settings'),
        '#description' => t('Settings specific to the chosen list handler.'),
        '#type' => 'fieldset',
        '#collapsible' => TRUE,
        '#collapsed' => TRUE,
        '#tree' => TRUE,
      );

      $form['handler_settings'] = array_merge($form['handler_settings'], $handler_settings_sub_form);
    }

    // Allow handler to override parts of the standard settings form. Useful if e.g max size is determined
    // by configuration of list backend.
    $list_handler->get_settings_override_form($form, $settings);

    $form['entity_support'] = array(
      '#title' => t('Supported content'),
      '#description' => t('Content that can be added to lists of this type'),
      '#type' => 'fieldset',
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      '#tree' => TRUE,
    );

    $handler_defs = $list_handler->get_supported_entity_handlers();

    foreach ($handler_defs as $id => $handler_def) {
      $form['entity_support'][$id] = array('#tree' => TRUE);
      $form['entity_support'][$id]['enabled'] = array(
        '#type' => 'checkbox',
        '#title' => $handler_def['label'],
        '#default_value' => !empty($settings['entity_support'][$id]['enabled']) ? $settings['entity_support'][$id]['enabled'] : FALSE,
      );

      $entity_settings = !empty($settings['entity_support'][$id]['settings']) ? $settings['entity_support'][$id]['settings'] : array();
      $entity_handler = $list_handler->get_supported_entity_handler($id);
      if($entity_handler && $handler_entity_settings_form = $entity_handler->get_settings_form($entity_settings)) {
        $form['entity_support'][$id]['settings'] = array(
          '#title' => t('@type settings', array('@type' => $handler_def['label'])),
          '#type' => 'fieldset',
          '#collapsible' => TRUE,
          '#collapsed' => TRUE,
          '#tree' => TRUE,
        );

        $form['entity_support'][$id]['settings'] = array_merge($form['entity_support'][$id]['settings'], $handler_entity_settings_form);
      }
    }

    $form['buttons']['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Save configuration'),
    );
  }

  return $form;
}

/**
 * Validation handler for lister_settings_form.
 */
function lister_settings_form_add_validate($form, &$form_state) {
  if (!preg_match('/^[a-z_][a-z0-9_]*$/', $form_state['values']['id'])) {
    form_set_error('id', t('The identifier may only contain lowercase letters, underscores, and numbers.'));
  }

  if (empty($form['#configuration_id'])) {
    // new configuration - check that hasn't been used before
    $existing_configuration = lister_get_configuration($form_state['values']['id']);
    if (!empty($existing_configuration)) {
      form_set_error('id', t('The identifier must be unique.'));
    }
    elseif ($form_state['values']['id'] == 'settings') {
      form_set_error('id', t('The identifier may not be "settings" - sorry!'));
    }
  }
}

/**
 * First step handler.
 */
function lister_settings_form_add_submit($form, &$form_state) {
  $form_state['storage']['id'] = $form_state['values']['id'];
  $form_state['storage']['handler_id'] = $form_state['values']['handler_id'];
}

/**
 * Submit handler for lister_settings_form.
 */
function lister_settings_form_submit($form, &$form_state) {
  // Exclude unnecessary elements.
  unset($form_state['values']['submit'], $form_state['values']['reset'], $form_state['values']['form_id'], $form_state['values']['op'], $form_state['values']['form_token'], $form_state['values']['form_build_id']);
  $configuration = array_merge($form_state['values'], array('id' => $form['#configuration_id'], 'handler_id' => $form['#handler_id']));

  lister_set_configuration($configuration['id'], $configuration);
  unset($form_state['storage']);

  $form_state['redirect'] = 'admin/settings/lister';
}

/**
 * Confirmation form for delete configuration operation.
 */
function lister_delete_form(&$form_state, $id) {
  $settings = lister_get_configuration($id);
  $form = array('#lid' => $id);
  $question = t('Delete list configuration "@name"?', array('@name' => $settings['label']));
  $path = 'admin/settings/lister';

  return confirm_form($form, $question, $path);
}

/**
 * Submit handler for delete configuration confirmation form.
 */
function lister_delete_form_submit($form, &$form_state) {
  $settings = lister_get_configuration($form['#lid']);
  if ($form_state['values']['confirm']) {
    lister_set_configuration($form['#lid']);
  }
  drupal_set_message(t('List configuration @name has been deleted.', array('@name' => $settings['label'])));
  $form_state['redirect'] = 'admin/settings/lister';
}

/**
 * Theme function for main flag recommender admin page.
 */
function theme_lister_admin_page($configurations = NULL) {
  $output = '';

  if(empty($configurations)) {
    $output .= t('Not yet configured. <a href="@path">Set up a list configuration</a>.', array('@path' => url('admin/settings/lister/add')));
  } else {
    foreach($configurations as $configuration) {

      $ops = array(
        'list_edit' =>  array('title' => t('edit'), 'href' => 'admin/settings/lister/edit/'.$configuration['id'], 'query' => drupal_get_destination()),
        'list_delete' =>  array('title' => t('delete'), 'href' => 'admin/settings/lister/delete/'.$configuration['id']),
      );

      $rows[] = array(
        $configuration['label'],
        $configuration['handler_label'],
        empty($configuration['max_per_user']) ? t('unlimited') : $configuration['max_per_user'],
        theme('links', $ops),
      );
    }

    $header = array(t('Label'), t('List handler'), t('Max lists per user'), t('Ops'));

    $output .= theme('table', $header, $rows);
  }

  return $output;
}

/**
 * Menu callback for lister global settings page.
 */
function lister_settings_admin_page() {
  return drupal_get_form('lister_global_settings_form');
}

/**
 * Form definition for global settings.
 */
function lister_global_settings_form(&$form_state) {
  $form = array();

  $settings = lister_get_global_configuration();

  $form['lister_settings'] = array(
    '#tree' => TRUE,
  );

  $form['lister_settings']['collate_links'] = array(
    '#type' => 'checkbox',
    '#title' => t('Collate links'),
    '#default_value' => $settings['collate_links'],
    '#description' => t('Group all list actions for all list types under 1 link. Otherwise a link will be displayed for each link type.'),
  );

  $form['lister_settings']['lots_of_lists'] = array(
    '#type' => 'textfield',
    '#title' => t('Large number of list actions threshold'),
    '#default_value' => $settings['lots_of_lists'],
    '#description' => t('This value may be used by theming functions to determine the point at which an alternative UI may be required to handle a large number of list actions.'),
  );

  return system_settings_form($form);
}
