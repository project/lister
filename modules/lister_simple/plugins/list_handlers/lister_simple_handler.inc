<?php
// $Id$
module_load_include('inc', 'lister', 'lister');

$plugin = array (
  'id' => 'lister_simple',
  'label' => t('Simple List'),
  'description' => t("A custom list implementation that maintains lists and entries in it's own database tables. Lists may have a title and description. List entries may have an accompanying message. List entries may be any entity type for which an entity handler is available (e.g. node/user/comment etc). Lists may contain entries of different entity types."),
  'handler' => 'lister_simple_handler',
);

class simple_lister_list_entry extends lister_list_entry {

  function __construct($list, $leid, $entity, $order = 0, $properties = array()) {
    parent::__construct($list, $leid, $entity, $order, $properties);
  }

  /**
   * Override to include the message with this one.
   */
  function display() {
    $output = '';
    if(!empty($this->properties['message'])) {
      $output .= '<div>'.$this->properties['message'].'</div>';
    }

    $output .= '<div>'.$this->entity->display().'</div>';

    return $output;
  }
}

class lister_simple_handler extends lister_handler {
  function __construct($conf) {
    parent::__construct($conf);
  }

  function create_list($properties) {
    if (empty($properties['title'])){
      $properties['title'] = $this->generate_list_title();
    }

    db_query("INSERT INTO {lister_simple_list} (type, title, uid, timestamp) VALUES ('%s', '%s', %d, %d)", $this->conf['id'], $properties['title'], $properties['user']->uid, time());
    $lid = db_last_insert_id('lister_simple_list', 'lid');
    return new lister_list($this, $lid, $properties);
  }

  /**
   * Save list properties and any updated entries.
   */
  function save_list($list) {
    if ($list->update_required) {
      db_query("UPDATE {lister_simple_list} SET title = '%s' WHERE lid = %d", $list->properties['title'], $list->id);
    }

    foreach ($list->get_entries() as $entry) {
      if ($entry->update_required) {
        $this->update_list_entry($list, $entry);
      }
    }
  }

  function get_list_form($list = NULL) {
    $form = array();
    $form['title'] = array(
      '#title' => t('Title'),
      '#type' => 'textfield',
    );

    if ($list) {
      $form['title']['#default_value'] = $list->get_title();
    }

    return $form;
  }

  function get_entry_form($list = NULL, $entry = NULL) {
    $form = array();
    $form['message'] = array(
      '#title' => t('Message'),
      '#type' => 'textarea',
      '#rows' => 3,
    );

    if ($entry) {
      $form['message']['#default_value'] = $entry->properties['message'];
    }

    return $form;
  }

  function submit_entry_form($form, $form_values, $entry = NULL) {
    return $form_values['properties'];
  }

  function submit_list_form($form, $form_values) {
    return $form_values;
  }

  function set_list_title($list, $title) {
    parent::set_list_title($list, $title);
    $list->update_required = TRUE;
  }

  function delete_list($list) {
    db_query("DELETE FROM {lister_simple_list_entry} WHERE lid = %d", $list->id);
    db_query("DELETE FROM {lister_simple_list} WHERE lid = %d", $list->id);
  }

  /**
   * remove a list entry from the list
   */
  function remove_list_entry($list, $list_entry) {
    db_query("DELETE FROM {lister_simple_list_entry} WHERE leid = %d", $list_entry->id);
  }

  /**
   * Add a list entry. Duplicate entry policy is to move to top of list.
   */
  function add_list_entry($list, $list_entry) {
    $entity = $list_entry['entity'];
    $id = $entity->get_id();
    $type = $entity->get_type();

    $entries = $list->get_entries();

    $min_weight = 0;
    $max_weight = 0;
    foreach ($entries as $existing_entry) {
      $min_weight = $min_weight < $existing_entry->order ? $min_weight : $existing_entry->order;
      $max_weight = $max_weight > $existing_entry->order ? $max_weight : $existing_entry->order;
    }

    $weight = $this->conf['new_entry_position'] == 'head' ? $min_weight - 1 : $max_weight + 1;

    db_query("INSERT INTO {lister_simple_list_entry} (lid, entity_id, entity_type, message, uid, weight, timestamp) VALUES (%d, %d, '%s', '%s', %d, %d, %d)", $list->id, $entity->get_id(), $entity->get_type(), $list_entry['properties']['message'], $list_entry['user']->uid, $weight, time());
    $leid = db_last_insert_id('lister_simple_list_entry', 'leid');

    return new simple_lister_list_entry($list, $leid, $list_entry);
  }

  function update_list_entry($list, $list_entry) {
    // $entries =& $list->get_entries();
    // $current_entry = $entries[$list_entry->id];
    // if ($current_entry->order <> $list_entry->order || $current_entry->properties['message'] <> $list_entry->properties['message']) {
    //   $entries[$list_entry->id] = $list_entry;
    //   $entries[$list_entry->id]->update_required = TRUE;
    // }
    db_query("UPDATE {lister_simple_list_entry} SET message = '%s', uid = %d, weight = %d, timestamp = %d WHERE leid = %d", $list_entry->properties['message'], $list_entry->uid, (int)$list_entry->order, time(), $list_entry->id);
    $list_entry->update_required = FALSE;
  }

  // function move_to_top($list, $list_entry) {
  //   $min_weight = db_result(db_query("SELECT MIN(weight) FROM {lister_simple_list_entry} WHERE lid = %d", $list->id));
  //
  //   if (isset($list_entry->id)) {
  //     db_query("UPDATE {lister_simple_list_entry} SET weight = %d WHERE leid = %d", $list_entry->id);
  //
  //
  //     $result = db_query("SELECT * FROM {lister_simple_list_entry} WHERE weight = ()
  //   }
  //
  // }

  /**
   * Return the list entries for a list
   */
  function get_list_entries($list) {
    $result = db_query("SELECT * FROM {lister_simple_list_entry} WHERE lid = %d ORDER BY weight", $list->id);
    $entries = array();
    while ($row = db_fetch_array($result)) {
      $entity_handler = lister_get_entity_handler($row['entity_type']);
      $entity = $entity_handler->construct_entity($row['entity_id']);
      $id = $row['leid'];
      $entries[$id] = new simple_lister_list_entry($list, $id, $entity, $row['weight'], $row);
    }
    return $entries;
  }

  /**
   * Return the count of list entries for a list
   */
  function get_list_stats($list) {
    $result = db_query("SELECT count(*) count FROM {lister_simple_list_entry} WHERE lid = %d", $list->id);
    if ($row = db_fetch_array($result)) {
      return $row;
    }
  }

  function get_list($lid) {
    $result = db_query("SELECT * FROM {lister_simple_list} WHERE lid = %d", $lid);
    if ($row = db_fetch_array($result)) {
      return new lister_list($this, $row['lid'], $row);
    }
  }

  function get_user_list_count($user) {
    $result = db_query("SELECT count(*) FROM {lister_simple_list} WHERE type = '%s' AND uid = %d", $this->conf['id'], $user->uid);
    return db_result($result);
  }

  function get_user_lists($user, $count = 0, $from = 0) {
    if ($count > 0) {
      $result = db_query_range("SELECT * FROM {lister_simple_list} WHERE type = '%s' AND uid = %d", $this->conf['id'], $user->uid, $from, $count);
    }
    else {
      $result = db_query("SELECT * FROM {lister_simple_list} WHERE type = '%s' AND uid = %d", $this->conf['id'], $user->uid);
    }
    $lists = array();
    while ($row = db_fetch_array($result)) {
      $lists[] = new lister_list($this, $row['lid'], $row);
    }

    return $lists;
  }

  // function display($list) {
  //   $output = '';
  //   $output .= '<ul>';
  //   $entries = $list->get_entries();
  //   foreach ($entries as $entry) {
  //     $output .= '<li>';
  //     $output .= '<div>'.$entry->display().'</div>';
  //     $output .= '</li>';
  //   }
  //   $output .= '</ul>';
  //   return $output;
  // }
}
