<?php
// $Id$

/**
 * @file
 * Provides JQuery UI Split Button and Dialog integration for lister.
 *
 * Features:
 * - Graceful JS degradation.
 * - Renders add to list links as buttons and split buttons.
 * - For optional entry/list details provides "add directly" and "add with details (...)" buttons.
 * - Add to list list/entry details and action completion message rendered in a dialog.
 * - Works with collated and discrete links for different list configurations.
 * - Edit entry links on list edit page open in a dialog.
 */

/**
 * Implementation of hook_menu().
 */
function lister_ui_menu() {
  // ajax version of add to list
  $items['lister/wf/%ctools_js/%/%'] = array(
    'title' => 'Add to list',
    'page callback' => 'lister_ui_workflow',
    'page arguments' => array(3,4,2),
    'access callback' => 'user_access',
    'access arguments' => array('access content'),
    'file' => 'lister_ui.page.inc',
    'type' => MENU_CALLBACK,
  );

  // ajax version of entry operation
  $items['lister_ui/entry/%ctools_js/%/%lister_conf/%/%'] = array(
    'title' => 'List entry operation',
    'page callback' => 'lister_ui_entry_op',
    'page arguments' => array(3,4,5,6,2),
    'access callback' => 'user_access',
    'access arguments' => array('access content'),
    'file' => 'lister_ui.page.inc',
    'type' => MENU_CALLBACK,
  );

  return $items;
}

/**
 * Implementation of hook_theme().
 */
function lister_ui_theme() {
  $path = drupal_get_path('module', 'lister_ui') .'/theme';

  return array(
    'lister_ui_entry_form' => array(
      'arguments' => array('form' => NULL),
      'file' => $path .'/lister_ui.theme.inc',
    ),
    'lister_ui_select_form' => array(
      'arguments' => array('form' => NULL),
      'file' => $path .'/lister_ui.theme.inc',
    ),
    'lister_ui_select_type_form' => array(
      'arguments' => array('form' => NULL),
      'file' => $path .'/lister_ui.theme.inc',
    ),
  );
}

/**
 * Implementation of hook_theme_registry_alter(). Overrides theme of links
 * provided by lister's hook_link() implementation.
 */
function lister_ui_theme_registry_alter(&$theme_registry) {
  $path = drupal_get_path('module', 'lister_ui') .'/theme';

  // render links as buttons/split buttons
  if (!empty($theme_registry['lister_links'])) {
    $theme_registry['lister_links']['function'] = 'theme_lister_ui_links';
    $theme_registry['lister_links']['file'] = $path .'/lister_ui.theme.inc';
  }
}

/**
 * Form alter for the multi-step "add list entry" form. Changes the action url
 * on the first step to ctools ajax friendly one.
 */
function lister_ui_form_lister_list_selection_form_alter(&$form, $form_state) {
  // changes the action url on the first step to ctools ajax friendly one.
  if (empty($form_state['storage'])) {
    $params = $form['#entity']->to_params();
    $params['destination'] = lister_get_destination();
    $form['#action'] = url('lister/wf/nojs/select/all', array('query' => $params));
  }

  // add ctools-dialog-button class to submit buttons
  foreach (element_children($form['list_types']) as $type) {
    foreach (element_children($form['list_types'][$type]['subform']['submits']) as $submit) {
      $form['list_types'][$type]['subform']['submits'][$submit]['#attributes']['class'] .= ' ctools-dialog-button';
    }
  }
}

/**
 * Form alter for the edit list form. Configures entry edit links to use a dialog.
 */
function lister_ui_form_lister_edit_list_form_alter(&$form, $form_state) {
  // Configure entry edit links to use a ctools dialog.
  lister_ui_dialog_add_js();
  $list = $form['#list'];
  foreach (element_children($form['entries']) as $entry_id) {
    unset($form['entries'][$entry_id]['properties']);
    $form['entries'][$entry_id]['ops']['#value']['entry_edit']['attributes']['class'] = 'ctools-use-dialog';
    $form['entries'][$entry_id]['ops']['#value']['entry_edit']['href'] = 'lister_ui/entry/nojs/edit/'.$list->handler->get_id().'/'.$list->id.'/'.$entry_id;
  }
}

/**
 * Form alter for the edit entry form. Configure to use ctools dialog.
 */
function lister_ui_form_lister_edit_entry_form_alter(&$form, $form_state) {
  // alter the entry edit form if being loaded into a dialog
  if (!empty($form_state['ajax'])) {
    // make the save button a dialog button
    $form['save']['#attributes']['class'] = 'ctools-dialog-button';

    // re-style the form to fit a dialog better
    $form['properties']['message']['#type'] = 'textarea';
    $form['properties']['message']['#rows'] = 3;
    $form['properties']['message']['#cols'] = 10;
    $form['properties']['message']['#resizable'] = TRUE;

    // add a cancel button
    $form['cancel'] = array(
      '#type' => 'button',
      '#value' => t('Cancel'),
      '#weight' => -1,
      '#attributes' => array(
        'class' => 'ctools-dialog-button ctools-use-dialog-processed',
        'onClick' => 'Drupal.Dialog.dismiss(); return false;',
      ),
    );

    $form['#theme'] = 'lister_ui_entry_form';
  }
}

/**
 * Return a ctools command to show a new or current dialog.
 */
function lister_ui_command_new_dialog() {
  return array(
    'command' => 'lister_ui_new_dialog',
  );
}

/**
 * Custom version of this to set dialog settings for lister.
 *
 * Add all the necessary javascript (and css) to be able to display a dialog
 * on the current page.  This must be used on any page that could possibly
 * contain a dialog.  It is safe to call this function repeatedly.
 */
function lister_ui_dialog_add_js() {
  // Provide a gate so we only do this once.
  static $done = FALSE;
  if ($done) {
    return;
  }

  // Include CTools ajax so it can preprocess the page's CSS and JS. Helps avoid
  // issues with cache clears.
  ctools_include('ajax');

  $settings = array(
    'Dialog' => array(
      'throbber' => theme('image', ctools_image_path('throbber.gif'), t('Loading...'), t('Loading')),
      'height' => variable_get('dialog_default_height', 'auto'),
      'width' => variable_get('dialog_default_width', '300px'),
    )
  );

  drupal_add_js($settings, 'setting');
  drupal_add_js('misc/jquery.form.js');
  ctools_add_js('ajax-responder');

  // Add jquery_ui js and css.
  jquery_ui_add(array('ui.core', 'ui.resizable', 'ui.draggable', 'ui.dialog'));

  // Get the correct CSS path based on jQuery UI version.
  $version_16 = version_compare(jquery_ui_get_version(), '1.7.0', '<');
  $css_path = $version_16 ? 'default' : 'base';
  $jquery_path = jquery_ui_get_path();
  drupal_add_css($jquery_path .'/themes/'. $css_path .'/jquery.ui.all.css');

  // And finally, the dialog js.
  drupal_add_js(drupal_get_path('module', 'dialog') .'/dialog.js');

  // Close the gate.
  $done = TRUE;
}




