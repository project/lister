List UI enhancements
====================
Open the following in popups:
Edit list entry form
Add list entry form
Add list form

Replace "add to list" links with split buttons.

Configuration
=============
jquery UI 1.8 uses different paths to previous versions. In particular CSS theme files have moved from themes/base/ui.*.css to themes/base/jquery.ui.*.css.

Set the default dialog width on the Dialog API configuration page.

Dependencies
============
Dialog
CTools
JQuery Update
JQuery UI with jquery UI 1.8+