<?php
/**
 * Menu handler for dialog based version of add entry multistep form.
 */
function lister_ui_workflow($step, $list_conf_id, $ajax) {
  global $user;
  module_load_include('inc', 'lister', 'lister.page');

  if (!$ajax || $step <> 'select') {
    return lister_workflow($step, $list_conf_id);
  }

  // get entity and relevant list confs for initial form
  if($entity = lister_get_entity_from_url()) {
    // select list - use form as allows for most flexible selection methods - e.g. autocomplete, checkbox etc
    $list_handlers = lister_get_handlers_for_entity($entity->get_type(), $entity->get_object());
  }

  ctools_include('ajax');

  if ($list_conf_id <> 'all' && isset($list_handlers[$list_conf_id])) {
    // single specified type form
    $list_handler = $list_handlers[$list_conf_id];

    $options = array('list_selection' => FALSE);
    $form_state = array(
      'ajax' => TRUE,
      'title' => t('Add to list'),
      'args' => array($entity, $list_handler, $user, $options),
    );
    $output = dialog_form_wrapper('lister_list_selection_type_form', $form_state);
  }
  else {
    // Set up form state for dialog form.
    $options = array('list_selection' => FALSE, 'max_selections' => 7);
    $form_state = array(
      'ajax' => TRUE,
      'title' => t('Add to list'),
      'args' => array($entity, $list_handlers, $user, $options),
    );

    $output = dialog_form_wrapper('lister_list_selection_form', $form_state);
  }

  // If form title provided - use it for the dialog title.
  if (!empty($form_state['storage']['form_title'])) {
    $output[0]['title'] = $form_state['storage']['form_title'];
  }

  // Handle form completion - display messages in dialog or redirect to landing page.
  if (empty($output)) {
    $messages = drupal_get_messages('status', FALSE);
    if(!empty($messages)) {
      // allow confirmation messages to be output etc.
      $dismiss = l(t('Close'), '', array('attributes' => array('onClick' => 'Drupal.Dialog.dismiss(); return false;')));

      // Re-assure the user that it's safe to close the dialog by providing a close link.
      $output[] = dialog_command_display(t('Done'), $dismiss);
    }
    else {
      // no messages - redirect to the "done" step (see below)
      $output[] = ctools_ajax_command_redirect($form_state['redirect'][0], 0, array('query' => $form_state['redirect'][1]));
    }
  }

  // ensure that there is a dialog to display in.
  array_unshift($output, lister_ui_command_new_dialog());

  // render!
  ctools_ajax_render($output);
}

function lister_ui_entry_op($op, $list_handler, $list_id, $entry_id, $ajax = FALSE) {
  module_load_include('inc', 'lister', 'lister.page');
  if (!$ajax || $op <> 'edit') {
    return lister_entry_op($op, $list_handler, $list_id, $entry_id);
  }

  $list = $list_handler->get_list($list_id);
  $entry = $list->get_entry($entry_id);

  ctools_include('ajax');

  // returns an array containing a dialog display command
  $form_state = array(
    'ajax' => TRUE,
    'title' => t('Edit Entry'),
    'args' => array($list, $entry),
  );
  $output = dialog_form_wrapper('lister_edit_entry_form', $form_state);
  if (empty($output)) {
    $output[] = dialog_command_display(t('Entry updated'), t('The list entry has been updated.'));
  }

  ctools_ajax_render($output);
}
