(function ($) {
  Drupal.CTools = Drupal.CTools || {};

  Drupal.behaviors.lister_ui = function(context) {
    $('.lister-sb-action:not(.lister-sb-processed)').addClass('lister-sb-processed').button()
      .each(function() {
        var button = '<button>'+$(this).val()+'</button>';
        // hide submit and add button after with click linked to submit
        $(this).hide().before(button).prev().button().click(function() {
          $(this).next().click();
          return false;
        });
      })
      .next()
      .button({text: false, icons: {primary: "ui-icon-triangle-1-s"}})
      .click(
        function() {
          var menu = $(this).parent().next();
          $('.lister-sb-menu').not(menu).hide();
          // add classes before positioning and reset position (css) before using ui.position() to ensure consistency
          // see http://forum.jquery.com/topic/position-utility-issues
          $(menu).addClass('ui-state-default ui-corner-left ui-corner-right').css({left:0, top:0}).toggle().position({my: "right top", at: "right bottom", of: this, offset: "0 0"});
          return false;
        }
      )
      .parent().buttonset();

    // hide open split button menu if window is resized
    $(window).resize(function() {
      $('.lister-sb-menu:visible').hide();
    });

    // make regular input:submit's into jquery ui buttons
    $('.lister-sb-menu-item input.form-submit.lister-sb-add').button();

    // replace add new input:submit with button with icon
    $('input.form-submit.lister-sb-addnew:not(.lister-sb-addnew-processed)')
    .addClass('lister-sb-addnew-processed')
    .each(function() {
      var button = '<button class="lister-sb-addnew">'+$(this).val()+'</button>';
      // hide submit and add button after with click linked to submit
      $(this).hide().after(button).next().button({icons: {primary: "ui-icon-plusthick"}}).click(function() {
        $(this).prev().click();
        return false;
      });
    })
    ;

    // replace direct add input:submit with button to align with ... button
    $('input.form-submit.lister-sb-predetail:not(.lister-sb-predetail-processed)')
    .addClass('lister-sb-predetail-processed')
    .each(function() {
      var button = '<button class="lister-sb-predetail">'+$(this).val()+'</button>';
      // hide submit and add button after with click linked to submit
      $(this).hide().before(button).prev().button().click(function() {
        $(this).next().click();
        return false;
      });
    })
    .next()
    // replace detail add input:submit with ... button and
    .each(function() {
      var button = '<button class="lister-sb-detail">...</button>';
      // hide submit and add button after with click linked to submit
      $(this).hide().after(button).next().button().click(function() {
        $(this).prev().click();
        return false;
      })
      ;
    })
    .parent().buttonset();

    // hide default nojs links
    $('a.lister-sb-default-link').hide();
    $('.lister-sb-splitbuttons').show();

    // Use CTools AJAX form processing which handles response commands to open dialog etc for entry details
    $('.lister-selection input.form-submit').click(function() {
      Drupal.CTools.AJAX.clickAJAXButton.apply(this);
      return false;
    });
  };

  // command to open/show dialog and hide any split button menu
  Drupal.CTools.AJAX.commands.lister_ui_new_dialog = function(command) {
    $('.lister-sb-menu:visible').hide();
    Drupal.Dialog.show();
  }
})(jQuery);
