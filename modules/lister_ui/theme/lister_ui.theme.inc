<?php
// $Id$

/**
 * Theme links for hook link display. Honours global lister settings:
 * - collate - present as single add to list link
 */
function theme_lister_ui_links($list_handlers, $entity, $options) {
  global $user;
  $links = array();
  $global_settings = lister_get_global_configuration();

  lister_ui_dialog_add_js();

  jquery_ui_add(array('ui.button', 'ui.position'));

  $jquery_ui_path = jquery_ui_get_path();
  drupal_add_css(drupal_get_path('module', 'lister') .'/theme/lister.css', 'module');
  drupal_add_css(drupal_get_path('module', 'lister_ui') .'/theme/lister_ui.css', 'module');

  // have to add this after ctools to ensure custom ctools commands are picked up
  drupal_add_js(drupal_get_path('module', 'lister_ui') . '/theme/lister_ui.js');

  module_load_include('inc', 'lister', 'lister.page');
  if (!empty($options['collate_links'])) {
    $links[] = array('title' => drupal_get_form('lister_list_selection_form', $entity, $list_handlers, $user, array('max_selections' => 4, 'theme' => 'lister_ui_select_form')), 'html' => TRUE);
  }
  else {
    module_load_include('inc', 'lister', 'lister.page');
    foreach ($list_handlers as $id => $list_handler) {
      $links[] = array('title' => drupal_get_form('lister_list_selection_form', $entity, array($id => $list_handler), $user, array('max_selections' => 4, 'theme' => 'lister_ui_select_form')), 'html' => TRUE);
    }
  }
  return $links;
}

/**
 * Theme a selection form as a list for rendering as a split button.
 */
function theme_lister_ui_select_type_form($form) {
  $list_type_form =& $form;
  $output .= '<ul>';
  $submits = element_children($list_type_form['subform']['submits']);
  foreach ($submits as $submit) {
    $submit_element =& $list_type_form['subform']['submits'][$submit];
    if (!$submit_element['#printed']) {
      if ($submit == 'add_new') {
        $submit_element['#attributes']['class'] .= ' lister-sb-addnew';
        $output .= '<li class="lister-sb-menu-item">'. drupal_render($submit_element);
      }
      elseif (in_array($submit.'_link_detail', $submits)) {
        // $list_type_form['subform']['submits'][$submit.'_link_detail']['#value'] = '...';
        $list_type_form['subform']['submits'][$submit]['#attributes']['class'] .= ' lister-sb-predetail';
        $list_type_form['subform']['submits'][$submit.'_link_detail']['#attributes']['class'] .= ' lister-sb-detail';

        $output .= '<li class="lister-sb-menu-item lister-sb-menu-item-detail">'. drupal_render($submit_element);
        $output .= drupal_render($list_type_form['subform']['submits'][$submit.'_link_detail']);
      }
      else {
        $submit_element['#attributes']['class'] .= ' lister-sb-add';
        $output .= '<li class="lister-sb-menu-item">'. drupal_render($submit_element);
      }
      $output .= '</li>';
      $count += 1;
    }
  }
  $output .= '</ul>';
  $output .= drupal_render($form);
  return $output;
}

/**
 * Main split button theme. Renders a default action button and a "more" button
 * to reveal other options which are designed to be grouped together in a jquery ui
 * buttonset.
 */
function theme_lister_ui_select_form($form) {
  $output = '';
  $list_type_elements = element_children($form['list_types']);

  // build up the form that will be revealed when the "more" button is clicked.
  $count = 0;
  $sub_output = '';
  foreach ($list_type_elements as $list_type) {
    $form['list_types'][$list_type]['#theme'] = 'lister_ui_select_type_form';
    $form['list_types'][$list_type]['#type'] = 'markup';
    $list_type_output = drupal_render($form['list_types'][$list_type]);
    if (count($list_type_elements) > 1) {
      $sub_output .= '<li class="lister-sb-menu-item"><span class="lister-sb-menu-item-label">'. $form['list_types'][$list_type]['#title'] .'</span>';
      $sub_output .= $list_type_output;
      $sub_output .= '</li>';
    }
    else {
      $sub_output .= '<li class="lister-sb-menu-item">';
      $sub_output .= $list_type_output;
      $sub_output .= '</li>';
    }
    $count += count(element_children($form['list_types'][$list_type]['subform']['submits']));
  }

  // set default to the first submit button of the first list type
  $first_list_type = reset($list_type_elements);
  $first_submit_id = reset(element_children($form['list_types'][$first_list_type]['subform']['submits']));
  $first_submit =& $form['list_types'][$first_list_type]['subform']['submits'][$first_submit_id];

  // render by default as link and let JS if enabled replace with split button
  $entity = $form['#entity'];
  $params = $entity->to_params();
  $params['destination'] = lister_get_destination();

  if (count($list_type_elements) > 1) {
    $output .= l(t('Add to list'), 'lister/wf/select/all', array('query' => $params, 'attributes' => array('class' => 'lister-sb-default-link')));
  }
  else {
    $list_handler = $form['list_types'][$first_list_type]['subform']['#list_handler'];
    $output .= l(t('Add to @list_type', array('@list_type' => $list_handler->conf['label'])), 'lister/wf/select/'.$list_handler->get_id(), array('query' => $params, 'attributes' => array('class' => 'lister-sb-default-link')));
  }

  // render form for button display - hidden by default - JS will replace links with rendered buttons
  $output .= '<span class="lister-sb-splitbuttons" style="display: none">';
  if ($count > 1) {
    $first_submits = element_children($form['list_types'][$first_list_type]['subform']['submits']);
    if ($count == 2 && in_array($first_submit_id.'_link_detail', $first_submits)) {
      // handle special case where two submit buttons are add entry directly, and add entry with comment (...)
      // no need for drop down
      $detail_submit =& $form['list_types'][$first_list_type]['subform']['submits'][$first_submit_id.'_link_detail'];

      $first_submit['#attributes']['class'] .= ' lister-sb-predetail';
      $detail_submit['#attributes']['class'] .= ' lister-sb-detail';

      $output .= '<span style="white-space: nowrap;">';
      $output .= drupal_render($first_submit);
      $output .= drupal_render($detail_submit);
      $output .= '</span>';
    }
    else {
      // render the form for a split button - lister-sb-action, lister-sb-down, lister-sb-menu.
      $first_submit['#attributes']['class'] .= ' lister-sb-action';
      $output .= '<span style="white-space: nowrap;">';
      $output .= drupal_render($first_submit);
      $output .= '<button class="lister-sb-down">More</button>';
      $output .= '</span>';
      // add a large class if required to add scrolling etc when appropriate.
      $output .= $count > 10 ? '<ul class="lister-sb-menu lister-sb-menu-large">' : '<ul class="lister-sb-menu">';
      $output .= $sub_output;
      $output .= '</ul>';
    }
  }
  else {
    // just one action render as normal
    $first_submit['#attributes']['class'] .= ' lister-sb-action';
    $output .= drupal_render($first_submit);
  }
  $output .= '</span>';

  // render the rest of the form
  $output .= drupal_render($form);
  return $output;
}

/**
 * Basic theme function to add a node title to the popup.
 */
function theme_lister_ui_entry_form($form) {
  $output .= '<div>';
  $output .= '<h3>'. $form['#entry']->get_title() .'</h3>';
  $output .= '<div>'.drupal_render($form).'</div>';
  $output .= '</div>';
  return $output;
}

