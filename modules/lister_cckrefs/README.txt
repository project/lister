Lister plugin for CCK reference field backed lists
==================================================

This module provides a list handler plugin that allows lister to work with any node type configured with a cck reference field. Lists are created as nodes and list entries are added to the list node's cck reference field.

Capabilities
------------
Works with CCK Nodereference and CCK Userreference fields.
Duplicates supported.
No user added attribution.
No entry details (currently).

How to use
----------
1. Either set up a new content type with a CCK reference field or use an existing content type that already has a CCK reference field.

2. The reference field widget should be "Autocomplete text field". The other widget types ("Select list" and "Check boxes/radio buttons") will remove duplicate entries and will not preserve ordering when content is edited.

3. Set up a list type with the "CCK Reference List" handler. On the list configuration page the key settings are under the "List handler settings" heading where you must set the "List node type" and the "The reference field".

4. Check that the reference field is set up to reference content/users of the type(s) you want to add lists of this list type.

Notes
-----
1. Filtering of referenceable content using views is not supported by this list handler.

2. A content type may only be used for 1 list type. (Hopefully this limitation can be overcome at some point.)

