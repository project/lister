<?php
// $Id$

/**
 * @file
 * List handler for CCK reference field backed lists. Includes entity handlers
 * for CCK node and user reference fields on which the cck list handler is dependent.
 */

$plugin = array (
  'id' => 'lister_refs',
  'label' => t('CCK Reference List'),
  'description' => t("A list type that uses nodes and cck reference fields to maintain lists."),
  'handler' => 'lister_refs_handler',
);

module_load_include('inc', 'lister', 'lister');
module_load_include('inc', 'lister', 'plugins/entity_handlers/lister_node_entity_handler');
module_load_include('inc', 'lister', 'plugins/entity_handlers/lister_user_entity_handler');

/**
 * A usort() callback to order entries by the order member.
 */
function _lister_cckrefs_sort_entries($entryA, $entryB) {
  return $entryA->order > $entryB->order;
}

class lister_refs_handler extends lister_handler {
  function __construct($conf) {
    parent::__construct($conf);
  }

  /**
   * Create a CCK reference based list - i.e. a node of the configured type.
   */
  function create_list($properties) {
    if (empty($properties['title'])){
      $properties['title'] = $this->generate_list_title();
    }

    // Create a new node - using the node form submission technique.
    $new_form_state = array();
    $list_node_type = $this->conf['handler_settings']['list_type'];
    module_load_include('inc', 'node', 'node.pages');
    $node = array('type' => $list_node_type);
    $new_form_state['values']['title'] = $properties['title'];
    $new_form_state['values']['name'] = $properties['user']->name;
    $new_form_state['values']['op'] = t('Save');
    drupal_execute($list_node_type.'_node_form', $new_form_state, (object)$node);

    $list_node = node_load($new_form_state['nid']);

    return new lister_list($this, $list_node->nid, $list_node);
  }

  /**
   * Save list properties and any updated entries (order only).
   */
  function save_list($list) {
    $list_node = node_load($list->get_id());
    $list_entries_field_name = $this->conf['handler_settings']['entries_field_name'];
    $list_entries_field_info = content_fields($list_entries_field_name);
    $column = reset(array_keys($list_entries_field_info['columns']));

    // resave entry order
    $entries = $list->get_entries();
    if (usort($entries, '_lister_cckrefs_sort_entries')) {
      $field = array();

      foreach ($entries as $entry) {
        $field[] = array($column => $entry->entity->get_id());
      }

      $list_node->$list_entries_field_name = $field;
    }

    // Resaving the updated node.
    $list_node = node_submit($list_node);
    node_save($list_node);
  }

  /**
   * Only show a list form when creating a new list ($list == NULL). For existing
   * lists the node edit page should be used.
   *
   * @todo Possibly allow users with list edit but not node edit to change
   * basic list info such as title and description.
   */
  function get_list_form($list = NULL) {
    if (!$list) {
      // list being created - prompt for title.
      $form = array();
      $form['#tree'] = TRUE;
      $form['title'] = array(
        '#title' => t('Title'),
        '#type' => 'textfield',
      );
      return $form;
    }
  }

  /**
   * Node delete will delete all entries as well (not the referenced nodes).
   */
  function delete_list($list) {
    node_delete($list->id);
  }

  /**
   * Remove a list entry from the list.
   *
   * @todo This is a lazy (developer) node save implementation which would be more efficiently
   * implemented as a database write.
   */
  function remove_list_entry($list, $list_entry) {
    $list_node = node_load($list->get_id());

    // alter the entries field
    $list_entries_field_name = $this->conf['handler_settings']['entries_field_name'];

    $id = $list_entry->get_id();
    $entries = $list_node->$list_entries_field_name;
    unset($entries[$id]);
    // set entries field to new set of entries with node removed
    $list_node->$list_entries_field_name = $entries;

    // Resaving the updated node.
    $list_node = node_submit($list_node);
    node_save($list_node);

    // clear cache
    db_query("DELETE FROM {cache_content} WHERE cid = '%s'", 'content:' . $list_node->nid . ':' . $list_node->vid);
  }

  /**
   * Add a list entry. All duplication/list full logic is handled by the list class
   * which calls this. It does however check whether to add to the head or tail of the
   * list.
   *
   * @todo This is a lazy (developer) node save implementation which would be more efficiently
   * implemented as a database write.
   */
  function add_list_entry($list, $list_entry) {
    $list_node = node_load($list->get_id(), NULL, TRUE);
    $list_entries_field_name = $this->conf['handler_settings']['entries_field_name'];
    $list_entries_field_info = content_fields($list_entries_field_name);
    $column = reset(array_keys($list_entries_field_info['columns']));


    $entity = $list_entry['entity'];
    $field = $list_node->$list_entries_field_name;

    $new_field_entry = array($column => $entity->get_id());
    if ($this->conf['new_entry_position'] == 'head') {
      array_unshift($field, $new_field_entry);
      $delta = 0;
    }
    else {
      $field[] = $new_field_entry;
      $delta = count($field)-1;
    }

    $list_node->$list_entries_field_name = $field;

    // Resaving the updated node.
    $list_node = node_submit($list_node);
    node_save($list_node);

    // clear cache
    db_query("DELETE FROM {cache_content} WHERE cid = '%s'", 'content:' . $list_node->nid . ':' . $list_node->vid);

    return new lister_list_entry($list, $delta, $list_entry);
  }

  /**
   * Get max size. Max size is determined by the CCK reference field setting
   * unless that is set to unlimited, in which case the list type setting is
   * used. This allows any size to be specified.
   */
  function get_max_list_size() {
    $list_entries_field_name = $this->conf['handler_settings']['entries_field_name'];
    $list_entries_field_info = content_fields($list_entries_field_name);

    // get max list size - multiple set to 1 means unlimited
    $max_size = $list_entries_field_info['multiple'] <= 1 ? $this->conf['max_list_size'] : $list_entries_field_info['multiple'];

    return $max_size;
  }

  /**
   * Return the list entries for a list
   */
  function get_list_entries($list) {
    $list_node = node_load($list->get_id());

    // alter the entries field
    $list_entries_field_name = $this->conf['handler_settings']['entries_field_name'];
    $list_entries_field_info = content_fields($list_entries_field_name);
    $column = reset(array_keys($list_entries_field_info['columns']));


    // recreate entries with node removed
    $entries = array();
    $entity_handler = $this->get_entity_handler($column == 'nid' ? 'node' : 'user');
    foreach($list_node->$list_entries_field_name as $delta => $entry) {
      if ($entry[$column]) {
        $entity = $entity_handler->construct_entity($entry[$column]);

        $entries[$delta] = new lister_list_entry($list, $delta, $entity, $delta);
      }
    }

    return $entries;
  }

  /**
   * Get the title from the node (stored in properties).
   */
  function get_list_title($list) {
    return $list->properties->title;
  }

  /**
   * Return the count of list entries for a list
   */
  function get_list_stats($list) {
    return array('count' => count($list->get_entries()));
  }

  /**
   * Get the list.
   *
   * @param $lid
   *  lid is the list node nid.
   */
  function get_list($lid) {
    $node = node_load($lid);
    return new lister_list($this, $lid, $node);
  }

  /**
   * Get count of users lists. Returns count of all users nodes of the configured list type.
   */
  function get_user_list_count($user) {
    $result = db_query("SELECT count(*) FROM {node} WHERE type = '%s' AND uid = %d", $this->conf['handler_settings']['list_type'], $user->uid);
    return db_result($result);
  }

  /**
   * Get users lists. Returns all users nodes of the configured list type.
   */
  function get_user_lists($user, $count = 0, $from = 0) {
    if ($count > 0) {
      $result = db_query_range("SELECT * FROM {node} WHERE type = '%s' AND uid = %d", $this->conf['handler_settings']['list_type'], $user->uid, $from, $count);
    }
    else {
      $result = db_query("SELECT * FROM {node} WHERE type = '%s' AND uid = %d", $this->conf['handler_settings']['list_type'], $user->uid);
    }
    $lists = array();
    while ($row = db_fetch_object($result)) {
      $lists[] = new lister_list($this, $row->nid, $row);
    }

    return $lists;
  }

  /**
   * Get list handler admin settings form. Allow admin to choose node type
   * and reference field.
   *
   * @todo Put some ahah form refresh in here.
   */
  function get_settings_form($settings) {
    $form = array();
    $form['list_type'] = array(
      '#type' => 'select',
      '#options' => array_map('check_plain', node_get_types('names')),
      '#title' => t('List node type'),
      '#description' => t('Select the list node type.'),
      '#default_value' => !empty($settings['list_type']) ? $settings['list_type'] : array(),
      '#required' => TRUE,
    );

    $form['list_meta_settings_start'] = array(
      '#value' => '<div id="list_meta_type_wrapper" class="inline-options">',
    );

    $list_meta_type = $settings['list_type'];

    if($list_meta_type) {
      // get reference fields
      // reference fields for node type
      $content_info = content_types($list_meta_type);
      $node_reference_options = array();
      foreach($content_info['fields'] as $field_name => $field_info) {
        if($field_info['type'] == 'nodereference' || $field_info['type'] == 'userreference') {
          $node_reference_options[$field_name] = $field_info['widget']['label'];
        }
      }

      if(!empty($node_reference_options)) {
        $entries_field_name = $settings['entries_field_name'];
        $form['entries_field_name'] = array(
          '#title' => 'The reference field',
          '#type' => 'select',
          '#options' => $node_reference_options,
          '#default_value' => $entries_field_name,
          '#description' => t('Select the reference field.'),
        );
      } else {
        drupal_set_message('Node type has no reference fields');
      }
    }

    $form['list_meta_settings_end'] = array(
      '#value' => '</div>',
    );

    return $form;
  }

  /**
   * Return the set of supported entity handlers for this list handler. Don't use
   * the entity plugin mechanism as these are particular to this list handler.
   */
  function get_supported_entity_handlers() {
    // make sure dependencies are loaded
    if (!empty($this->conf['handler_settings']['entries_field_name'])) {
      if ($list_entries_field_info = content_fields($this->conf['handler_settings']['entries_field_name'])) {
        if ($list_entries_field_info['type'] == 'userreference') {
          return array(
            'lister_cckrefs_userref_entity_handler' => array(
              'id' => 'userref',
              'type' => 'user',
              'label' => t('Users (by cck user reference field)'),
              'handler' => 'lister_cckrefs_userref_entity_handler',
            ),
          );
        }
        elseif ($list_entries_field_info['type'] == 'nodereference') {
          return array(
            'lister_cckrefs_noderef_entity_handler' => array(
              'id' => 'noderef',
              'type' => 'node',
              'label' => t('Nodes (by cck node reference field)'),
              'handler' => 'lister_cckrefs_noderef_entity_handler',
            ),
          );
        }
      }
    }
    return array();
  }

  /**
   * Get a configured instance of an entity handler. Gives the list handler a chance
   * to configure the handler.
   */
  function get_supported_entity_handler($id) {
    $entity_handler_defs = $this->get_supported_entity_handlers();
    // check handler exists and list handler contains necessary configuration
    if (!empty($entity_handler_defs[$id]) && !empty($this->conf['handler_settings']['entries_field_name'])) {
      $list_entries_field_name = $this->conf['handler_settings']['entries_field_name'];
      // get content field info
      if ($list_entries_field_info = content_fields($list_entries_field_name)) {
        return new $entity_handler_defs[$id]['handler'](array('field_info' => $list_entries_field_info));
      }
    }
  }

  /**
   * Default list view page is the list node view.
   */
  function get_url($list) {
    return 'node/'.$list->get_id();
  }

  /**
   * List admin form alter. Conditionally disable settings that are set by
   * the CCK field configuration.
   */
  function get_settings_override_form(&$form, $settings) {
    if (!empty($settings['handler_settings']['entries_field_name'])) {
      $list_entries_field_info = content_fields($settings['handler_settings']['entries_field_name']);

      if ($list_entries_field_info['multiple'] > 1) {
        $description = t('This value can only be set when the entries reference field is set to unlimited (currently !field_multiple). ', array('!field_multiple' => $list_entries_field_info['multiple']));
        $url = url('/admin/content/node-type/'. $settings['handler_settings']['list_type'] .'/fields/'. $settings['handler_settings']['entries_field_name'], array('query' => drupal_get_destination()));
        $description .= t('Field settings can changed on the content type\'s <a href="@field_page_url">field management page</a>. ', array('@field_page_url' => $url));

        $form['list_behaviour']['max_list_size']['#description'] = $description;
        $form['list_behaviour']['max_list_size']['#default_value'] = $this->get_max_list_size();
        $form['list_behaviour']['max_list_size']['#disabled'] = TRUE;
      }
    }

    $form['ui_settings']['add_entry_form']['#disabled'] = TRUE;
    $form['ui_settings']['add_entry_form']['#description'] = t('<strong>Entry details are not supported by this list handler.</strong> ');
    $form['ui_settings']['add_entry_form']['#default_value'] = 'never';
    $form['ui_settings']['add_entry_form']['#value'] = 'never';
  }
}

/**
 * Implements lister_entity_handler for nodes that will be referenced by a cck node reference field.
 * Should be constructed with 'field_info' in the settings array.
 */
class lister_cckrefs_noderef_entity_handler extends node_lister_entity_handler {
  function get_settings_form($settings) {
    $form = array();

    if (!empty($this->settings['field_info'])) {
      $url = url('admin/content/node-type/'. $this->settings['field_info']['type_name'] .'/fields/'. $this->settings['field_info']['field_name'], array('query' => drupal_get_destination()));
      $referenceable_types = array_filter($this->settings['field_info']['referenceable_types']);

      $description = t('These options are set by the CCK node reference field settings.');
      $description .= t('Field settings can changed on the content type\'s <a href="@field_page_url">field management page</a>. ', array('@field_page_url' => $url));


      $form['types'] = array(
        '#type' => 'checkboxes',
        '#disabled' => TRUE,
        '#options' => array_map('check_plain', node_get_types('names')),
        '#title' => t('Content (Node) types'),
        '#description' => $description,
        '#default_value' => $referenceable_types,
      );
    }
    else {
      $form['message'] = array(
        '#value' => t('Supported entities are determined by the selected reference field.'),
      );
    }

    return $form;
  }

  /**
   * Override is_entity_supported to delegate to the CCK field settings. Don't
   * fully delegate as that would entail running a query and or a view to get the
   * set of allowed nodes.
   */
  function is_entity_supported($type, $object) {
    $referenceable_types = array_filter($this->settings['field_info']['referenceable_types']);
    return $type == 'node' && in_array($object->type, $referenceable_types);
  }
}

/**
 * Implements lister_entity_handler for nodes that will be referenced by a cck node reference field.
 * Should be constructed with 'field_info' in the settings array.
 */
class lister_cckrefs_userref_entity_handler extends user_lister_entity_handler {
  function get_settings_form($settings) {
    $form = array();

    if (!empty($this->settings['field_info'])) {
      $url = url('admin/content/node-type/'. $this->settings['field_info']['type_name'] .'/fields/'. $this->settings['field_info']['field_name'], array('query' => drupal_get_destination()));

      if (module_exists('userreference')) {
        $form = userreference_field_settings('form', $this->settings['field_info']);
        unset($sub_form['advanced']);
        $form['referenceable_roles']['#disabled'] = TRUE;
        $form['referenceable_status']['#disabled'] = TRUE;
      }

      $description = t('These options are set by the CCK node reference field settings.');
      $description .= ' '.t('Field settings can changed on the content type\'s <a href="@field_page_url">field management page</a>. ', array('@field_page_url' => $url));

      $form['#description'] = $description;
    }
    else {
      $form['message'] = array(
        '#value' => t('Supported entities are determined by the selected reference field.'),
      );
    }

    return $form;
  }

  /**
   * Override is_entity_supported to delegate to the CCK field settings. Don't
   * fully delegate as that would entail running a query and or a view to get the
   * set of allowed nodes.
   */
  function is_entity_supported($type, $object) {
    if ($type == 'user') {
      if ($field = $this->settings['field_info']) {
        if (isset($field['referenceable_roles']) && is_array($field['referenceable_roles'])) {
          // keep only selected checkboxes
          $roles = array_filter($field['referenceable_roles']);
          // filter invalid values that seems to get through sometimes ??
          $roles = array_intersect(array_keys(user_roles(1)), $roles);
          if (!empty($roles) && !in_array(DRUPAL_AUTHENTICATED_RID, $roles)) {
            // if not one of these roles then return FALSE
            $matching_roles = array_intersect(array_keys($roles), array_keys($object->roles));
            if (empty($matching_roles)) {
              return FALSE;
            }
          }
        }

        if (isset($field['referenceable_status']) && is_numeric($field['referenceable_status'])) {
          // if not one of these statuses then return FALSE
          if ($field['referenceable_status'] <> $object->status) {
            return FALSE;
          }
        }
      }
      return TRUE;
    }
  }
}



