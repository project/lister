The Lister Module
=================

Lister is a comprehensive list building module. It deals with a number of common structural and usability issues specific to lists whilst allowing for different backends and extension for specific purpose.

Background
----------
There are a number of modules and techniques that can be used to build lists but none that address all the unique challenges of list building.

Features
--------
- List as a node - supports full and configurable list meta data.
- Support different types of list entries - i.e. node, users, terms.
- List entries may have associated meta data.
- Arbitrary list entry ordering.
- UI to facilitate adding content to lists.
- UI to allow on-the-fly list creation.
- UI to support entry of list and list entry meta-data.
- Allow user to choose whether to enter list and list entry meta-data (configurable)
- Allow user to create and maintain an arbitrary number of lists.
- Support specialised list types.
- Support aggregation of list entry data and meta-data.
- Support one-click list creation/list entry addition.

Basic list configuration - list types
-------------------------------------
- Create different list types for different needs - e.g. watchlist, wishlist, research, top 10s, listmania, gift ideas list, gift registries etc.

Pluggable list handlers
=======================
List handlers provide, or act as a bridge to, underlying list implementations. When acting as a bridge, the list implementation may be another Drupal collection-type implementation (e.g. CCK node references, flag, nodequeue etc) or an external list service (e.g. wishpot). The module comes with two sub-modules that provide the following handlers:

Simple List
-----------
A fresh list implementation which stores list details in tables. Includes support for basic list details (title, description), entry details (comment), mixed list content. Easy to set up.

CCK/Field References List
-------------------------
Uses CCK reference field to maintain lists. Lists may be of any designated node type that contains a cck reference field.

+ Add list functionality to existing cck reference field content.
+ Every list is node - many advantages.
- Does not support entry details.
- List entries of a single entity type.

List configuration
==================
List content
------------
- Select what may be added to a list (depends on the list backend e.g. a simple list allows any supported entity to be added to a list, a CCK node reference backed list will only allow nodes of types configured for the node reference field to be added).

List behaviour
--------------
- Choose whether new entries are added to the head or tail of the list.
- Set the maximum size of a list.
- Choose what to do when user attempts to add an entry to a full list: Prevent or remove last entry (effectively a queue).
- Duplicate entry policy: what to do when user attempts to add a duplicate entry to a list: Add duplicate, Don't add, Don't add but move existing entry to the new entry position.

List UI settings
----------------
- Prompt for new list details when add to new.
- Prompt for entry details when adding a list entry.
- Whether to redirect to a landing page when an entry has been added to a list.






