<?php

// $Id$

/**
 * @file
 * Entity handler for comments.
 */

module_load_include('inc', 'lister', 'lister');

$plugin = array (
  'id' => 'comment',
  'type' => 'comment',
  'label' => t('Comments'),
  'handler' => 'comment_lister_entity_handler',
);

/**
 * Implements lister_entity_handler for comment entities.
 */
class comment_lister_entity_handler extends lister_entity_handler {
  function construct_entity_obj($object) {
    return new lister_entity($object->cid, $this);
  }

  function get_title($id) {
    $comment = _comment_load($id);
    $node = node_load($comment->nid);
    $username = $this->_get_username($comment);
    return $comment->subject;
    // return t('Comment by !user on !node', array('!user' => $username, '!node' => $node->title));
  }

  function display($id, $context) {
    $comment = _comment_load($id);
    $node = node_load($comment->nid);

    if ($context == 'edit') {
      return l($this->get_title($id), 'node/'.$comment->nid, array('fragment' => 'comment-'.$id));
    }
    else {
      return comment_render($node, $id);
    }
  }

  function to_params($id) {
    return array('et' => 'comment', 'eid' => $id);
  }
  function get_type($id) {
    return 'comment';
  }
  function get_object($id) {
    return _comment_load($id);
  }
  function get_url($id) {
    $comment = _comment_load($id);

    return 'node/'.$comment->nid.'#comment-'.$id;
  }

  function _get_username($object) {
    if ($object->uid && $object->name) {
      // Shorten the name when it is too long or it will break many tables.
      if (drupal_strlen($object->name) > 20) {
        $name = drupal_substr($object->name, 0, 15) .'...';
      }
      else {
        $name = $object->name;
      }
      $output = check_plain($name);
    }
    else if ($object->name) {
      // Sometimes modules display content composed by people who are
      // not registered members of the site (e.g. mailing list or news
      // aggregator modules). This clause enables modules to display
      // the true author of the content.
      $output = check_plain($object->name);
      $output .= ' ('. t('not verified') .')';
    }
    else {
      $output = check_plain(variable_get('anonymous', t('Anonymous')));
    }

    return $output;
  }

  function is_entity_supported($type, $object) {
    return $type == 'comment';
  }
}

