<?php

// $Id$

/**
 * @file
 * Entity handler for users.
 */

module_load_include('inc', 'lister', 'lister');

$plugin = array (
  'id' => 'user',
  'type' => 'user',
  'label' => t('Users'),
  'handler' => 'user_lister_entity_handler',
);

/**
 * Implements lister_entity_handler for user entities.
 */
class user_lister_entity_handler extends lister_entity_handler {
  function construct_entity_obj($object) {
    return new lister_entity($object->uid, $this);
  }

  function get_title($id) {
    $account = user_load($id);
    return $account->name;
  }

  function display($id, $context) {
    $account = user_load($id);
    if ($context == 'edit') {
      return l($account->name, 'user/'.$id);
    }
    else {
      return user_view($account);
    }
  }
  function to_params($id) {
    return array('et' => 'user', 'eid' => $id);
  }
  function get_type($id) {
    return 'user';
  }
  function get_object($id) {
    return user_load($id);
  }
  function get_url($id) {
    return 'user/'.$id;
  }
  function is_entity_supported($type, $object) {
    return $type == 'user';
  }
}

