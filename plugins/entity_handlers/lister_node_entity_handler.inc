<?php

// $Id$

/**
 * @file
 * Entity handler for nodes.
 */

module_load_include('inc', 'lister', 'lister');

$plugin = array (
  'id' => 'node',
  'type' => 'node',
  'label' => t('Nodes'),
  'handler' => 'node_lister_entity_handler',
);

/**
 * Implements lister_entity_handler for node entities.
 */
class node_lister_entity_handler extends lister_entity_handler {
  var $types = array();

  function construct_entity_obj($object) {
    return new lister_entity($object->nid, $this);
  }

  function get_title($id) {
    $node = node_load($id);
    return $node->title;
  }

  function display($id, $context) {
    $node = node_load($id);

    if ($context == 'edit') {
      return l($node->title, 'node/'.$id);
    }
    else {
      return node_view($node, TRUE);
    }
  }

  function to_params($id) {
    return array('et' => 'node', 'eid' => $id);
  }
  function get_type($id) {
    return 'node';
  }
  function get_object($id) {
    return node_load($id);
  }
  function get_url($id) {
    return 'node/'.$id;
  }

  function get_settings_form($settings) {
    dpm($settings);
    $form = array();
    $form['types'] = array(
      '#type' => 'checkboxes',
      '#options' => array_map('check_plain', node_get_types('names')),
      '#title' => t('Content (Node) types'),
      '#description' => t('Select node types to limit nodes that may be added to lists of this type. If none selected, nodes of all types may be added.'),
      '#default_value' => !empty($settings['types']) ? $settings['types'] : array(),
      '#element_validate' => array('_lister_checkboxes_validate'),
    );
    return $form;
  }

  function is_entity_supported($type, $object) {
    return $type == 'node' && in_array($object->type, $this->settings['types']);
  }
}
