<?php
// $Id$

/**
 * @file
 * Lister module.
 *
 * @todo permissions
 * @todo check flexibility of theming
 * @todo compatibility with cacheing (authcache compatible per-role links).
 * @todo tidy up.
 * @todo views support
 */

/**
 * Implementation of hook_menu().
 */
function lister_menu() {
  $items = array();

  $items['lister/wf/%/%'] = array(
    'title' => 'Add to list',
    'page callback' => 'lister_workflow',
    'page arguments' => array(2,3),
    'access callback' => 'user_access',
    'access arguments' => array('access content'),
    'file' => 'lister.page.inc',
    'type' => MENU_CALLBACK,
  );

  $items['lister/entity/%/%/%lister_list'] = array(
    'title' => 'List entity operation',
    'page callback' => 'lister_entity_op',
    'page arguments' => array(2,4),
    'access callback' => 'user_access',
    'access arguments' => array('access content'),
    'load arguments' => array(3),
    'file' => 'lister.page.inc',
    'type' => MENU_CALLBACK,
  );

  $items['lister/list/%/%lister_list'] = array(
    'title callback' => 'lister_list_op_title',
    'title arguments' => array('show',3),
    'page callback' => 'lister_list_op',
    'page arguments' => array('show',3),
    'access callback' => 'user_access',
    'access arguments' => array('access content'),
    'load arguments' => array(2),
    'file' => 'lister.page.inc',
    'type' => MENU_CALLBACK,
  );
  $items['lister/list/%/%lister_list/view'] = array(
    'title' => 'View',
    'page callback' => 'lister_list_op',
    'page arguments' => array('show',3),
    'access callback' => 'user_access',
    'access arguments' => array('access content'),
    'load arguments' => array(2),
    'file' => 'lister.page.inc',
    'type' => MENU_DEFAULT_LOCAL_TASK,
    'weight' => -10
  );
  $items['lister/list/%/%lister_list/edit'] = array(
    'title' => 'Edit',
    // 'title callback' => 'lister_list_op_title',
    // 'title arguments' => array('edit',3),
    'page callback' => 'lister_list_op',
    'page arguments' => array('edit',3),
    'access callback' => 'user_access',
    'access arguments' => array('access content'),
    'load arguments' => array(2),
    'file' => 'lister.page.inc',
    'type' => MENU_LOCAL_TASK,
  );
  $items['lister/list/%/%lister_list/delete'] = array(
    'title' => 'Delete',
    // 'title callback' => 'lister_list_op_title',
    // 'title arguments' => array('edit',3),
    'page callback' => 'lister_list_op',
    'page arguments' => array('delete',3),
    'access callback' => 'user_access',
    'access arguments' => array('access content'),
    'load arguments' => array(2),
    'file' => 'lister.page.inc',
    'type' => MENU_CALLBACK,
  );
  $items['lister/entry/%/%/%lister_list/%'] = array(
    'title' => 'List entry operation',
    'page callback' => 'lister_entry_op',
    'page arguments' => array(2,4,5),
    'access callback' => 'user_access',
    'access arguments' => array('access content'),
    'load arguments' => array(3),
    'file' => 'lister.page.inc',
    'type' => MENU_CALLBACK,
  );

  $items['user/%/listerlists'] = array(
    'title' => 'My Lists',
    'page callback' => 'lister_my_lists_page',
    'page arguments' => array(1),
    'access callback' => 'user_access',
    'access arguments' => array('access content'),
    'file' => 'lister.page.inc',
    'type' => MENU_LOCAL_TASK,
  );

  $items['admin/settings/lister'] = array(
    'title' => t('Lister'),
    'description' => 'Configuration of lists',
    'page callback' => 'lister_admin_page',
    'access arguments' => array('administer'),
    'file' => 'lister.admin.inc',
  );

  $items['admin/settings/lister/list'] = array(
    'title' => t('List'),
    'type' => MENU_DEFAULT_LOCAL_TASK,
    'weight' => -1,
  );

  $items['admin/settings/lister/add'] = array(
    'title' => t('Add'),
    'description' => 'Add a list configuration',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('lister_settings_form'),
    'access arguments' => array('administer'),
    'file' => 'lister.admin.inc',
    'type' => MENU_LOCAL_TASK,
  );

  $items['admin/settings/lister/settings'] = array(
    'title' => t('Settings'),
    'description' => 'Global list settings',
    'page callback' => 'lister_settings_admin_page',
    'access arguments' => array('administer'),
    'file' => 'lister.admin.inc',
    'type' => MENU_LOCAL_TASK,
  );

  $items['admin/settings/lister/edit/%'] = array(
    'title' => t('Edit list configuration'),
    'page callback' => 'drupal_get_form',
    'page arguments' => array('lister_settings_form', 4),
    'access arguments' => array('administer'),
    'file' => 'lister.admin.inc',
    'type' => MENU_CALLBACK,
  );

  $items['admin/settings/lister/delete/%'] = array(
    'title' => t('Delete list configuration'),
    'page callback' => 'drupal_get_form',
    'page arguments' => array('lister_delete_form', 4),
    'access arguments' => array('administer'),
    'file' => 'lister.admin.inc',
    'type' => MENU_CALLBACK,
  );

  return $items;
}

/**
 * Menu title handler for list operations - edit/delete/show.
 */
function lister_list_op_title($op, $list) {
  return $list->get_title();
}

/**
 * Implementation of hook_link(). Return links to perform list actions by the
 * the user/role for the object. Actions may include add to new list, add to/remove
 * from existing list (enumerated). How the links are presented will be theme
 * and list configuration dependent. Defer link rendering to theme layer. Theme
 * layer gets the complete set of actions and configuration to decide how to display.
 *
 * List configuration options:
 * - show links
 * -
 */
function lister_link($type, $object, $teaser = FALSE) {
  // all link creation is delegated to theme layer for maximum flexibility
  // may return single add to list link, or per configuration e.g. add to list type A, add to list type B
  module_load_include('inc', 'lister', 'lister');

  // get list handlers configured to accept this object
  $handlers = lister_get_handlers_for_entity($type, $object);

  // Apply per role filtering here.
  // Per user filtering is up to theme layer depending on optimisation/caching requirements.

  // Theme gets to choose whether to ignore configuration options or not.
  if (!empty($handlers)) {
    $entity = lister_get_entity($type, $object);
    return theme('lister_links', $handlers, $entity, lister_get_global_configuration());
  }
}

/**
 * Implementation of hook_user.
 */
function lister_user($op, &$edit, &$edit_user, $category = NULL) {
  if ($op == 'view') {
    if ($links = lister_link('user', $edit_user)) {
      $edit_user->content['lister'] = array(
        '#value' => theme('links', $links, array('class' => 'links inline')),
        '#weight' => 11,
      );
    }
  }
}

/**
 * Get entity handlers for the given type and object.
 */
function lister_get_handlers_for_entity($type, $object) {
  module_load_include('inc', 'lister', 'lister');
  return _lister_get_handlers_for_entity($type, $object);
}

/**
 * Loader function for the %lister_conf argument.
 */
function lister_conf_load($id) {
  module_load_include('inc', 'lister', 'lister');
  return lister_get_handler($id);
}

/**
 * Loader function for the %lister_list argument. Needs the conf argument as well.
 */
function lister_list_load($list_id, $list_conf_id) {
  module_load_include('inc', 'lister', 'lister');
  $list_handler = lister_get_handler($list_conf_id);
  return $list_handler->get_list($list_id);
}

/**
 * Implementation of hook_theme().
 */
function lister_theme() {
  $path = drupal_get_path('module', 'lister') .'/theme';

  return array(
    'lister_links' => array(
      'arguments' => array('list_confs' => NULL, 'type' => NULL, 'object' => NULL, 'options' => array()),
      'file' => $path .'/lister.theme.inc',
    ),
    'lister_lists' => array(
      'arguments' => array('list_confs' => NULL, 'user' => NULL, 'options' => array()),
      'template' => 'lister_lists',
      'pattern' => 'lister_lists__',
      'file' => 'lister.theme.inc',
      'path' => $path,
    ),
    'lister_list' => array(
      'arguments' => array('list' => NULL, 'options' => array()),
      'template' => 'lister_list',
      'pattern' => 'lister_list__',
      'file' => 'lister.theme.inc',
      'path' => $path,
    ),
    'lister_list_page' => array(
      'arguments' => array('list' => NULL, 'options' => array()),
      'template' => 'lister_list_page',
      'pattern' => 'lister_list_page__',
      'file' => 'lister.theme.inc',
      'path' => $path,
    ),
    'lister_edit_list' => array(
      'arguments' => array('list' => NULL, 'options' => array()),
      'file' => 'lister.theme.inc',
      'path' => $path,
    ),
    'lister_form_ops' => array(
      'arguments' => array('form' => NULL),
      'file' => 'lister.theme.inc',
      'path' => $path,
    ),
    'lister_admin_page' => array(
      'arguments' => array('configurations' => NULL),
      'file' => 'lister.admin.inc',
    ),
    'lister_list_selection_type_form' => array(
      'arguments' => array('form' => NULL),
      'file' => 'lister.theme.inc',
      'path' => $path,
    ),
  );
}

/**
 * Return current destination path. Unencoded for adding to arrays of parameters and passed to l() etc.
 */
function lister_get_destination() {
  if (isset($_REQUEST['destination'])) {
    return $_REQUEST['destination'];
  }
  else {
    // Use $_GET here to retrieve the original path in source form.
    $path = isset($_GET['q']) ? $_GET['q'] : '';
    $query = drupal_query_string_encode($_GET, array('q'));
    if ($query != '') {
      $path .= '?' . $query;
    }
    return $path;
  }
}

function lister_get_lists($id, $user) {
  $list_handler = lister_get_handler($id);
  return $list_handler->get_user_lists($user);
}

function lister_get_entity_params($entity) {
  $params = array('et' => $entity['type']);

  if (!empty($entity['eid'])) {
    $params['eid'] = $entity['eid'];
  }
  elseif (!empty($entity['object'])) {
    switch ($entity['type']) {
      case 'node':
        $params['eid'] = $entity['object']->nid;
        break;
      default;
    }
  }

  return $params;
}

function lister_get_entity_from_url() {
  if (!empty($_GET['et']) && !empty($_GET['eid'])) {
    $handler = lister_get_entity_handler($_GET['et']);
    return $handler->construct_entity($_GET['eid']);
  }
}

function lister_get_list_from_url() {
  if (!empty($_GET['lt']) && !empty($_GET['lid'])) {
    $list_handler = lister_get_handler($_GET['lt']);
    return $list_handler->get_list($_GET['lid']);
  }
}

/**
 * Implementation of hook_ctools_plugin_directory.
 */
function lister_ctools_plugin_directory($module, $plugin) {
  if ($module == 'lister') {
    return 'plugins/' .$plugin;
  }
}


